Day 17: Set and Forget
======================

https://adventofcode.com/2019/day/17

I could've done part 2 of this one by hand, but I decided writing a horrible brute force search algorithm that tries to compress the path by going through every possible combination of three subpaths would be more fun! It was certainly way more code, at least.
