#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "17.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


struct VM {
    enum State { case output(Int), waitingForInput, halted }

    var program: [Int]
    var inputs: [Int] = []

    var pc = 0
    var relativeBase = 0


    mutating func extendMemory(toLocation memoryLocation: Int) {
        if memoryLocation >= program.count {
            program += Array(repeating: 0, count: (memoryLocation - program.count + 1))
        }
    }

    mutating func write(value: Int, toMemoryLocation memoryLocation: Int) {
        extendMemory(toLocation: memoryLocation)
        program[memoryLocation] = value
    }

    mutating func getParameter(atIndex index: Int, withMode mode: Int) -> Int {
        if mode == 1 {
            // mode 1: immediate
            return program[index]
        }

        // mode 0: position
        // mode 2: relative
        let memoryLocation = (mode == 0) ? program[index] : (program[index] + relativeBase)
        extendMemory(toLocation: memoryLocation)
        return program[memoryLocation]
    }

    func getWriteLocation(atIndex index: Int, withMode mode: Int) -> Int {
        if mode == 0 {
            // mode 0: position
            return program[index]
        } else {
            // mode 2: relative
            return program[index] + relativeBase
        }
    }

    mutating func execute() -> State {
        outer: while pc < program.count {
            let opcode = program[pc] % 100
            let parameter1Mode, parameter2Mode, parameter3Mode: Int
            if program[pc] > 100 {
                parameter1Mode = (program[pc] / 100) % 10
            } else {
                parameter1Mode = 0
            }
            if program[pc] > 1000 {
                parameter2Mode = (program[pc] / 1000) % 10
            } else {
                parameter2Mode = 0
            }
            if program[pc] > 10000 {
                parameter3Mode = (program[pc] / 10000) % 10
            } else {
                parameter3Mode = 0
            }

            switch opcode {
            case 1:
                // +
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
                write(value: operand1 + operand2, toMemoryLocation: location)
                pc += 4
            case 2:
                // *
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
                write(value: operand1 * operand2, toMemoryLocation: location)
                pc += 4
            case 3:
                // input
                if inputs.isEmpty {
                    return .waitingForInput
                } else {
                    let input = inputs.removeFirst()
                    let location = getWriteLocation(atIndex: pc + 1, withMode: parameter1Mode)
                    write(value: input, toMemoryLocation: location)
                    pc += 2
                }
            case 4:
                // output
                let output = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                pc += 2
                return .output(output)
            case 5:
                // jump if true
                let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                if condition != 0 {
                    pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                } else {
                    pc += 3
                }
            case 6:
                // jump if false
                let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                if condition == 0 {
                    pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                } else {
                    pc += 3
                }
            case 7:
                // less than
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
                write(value: (operand1 < operand2) ? 1 : 0, toMemoryLocation: location)
                pc += 4
            case 8:
                // equals
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
                write(value: (operand1 == operand2) ? 1 : 0, toMemoryLocation: location)
                pc += 4
            case 9:
                // update relative base
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                relativeBase += operand1
                pc += 2
            case 99:
                // halt
                break outer
            default:
                // oh no
                print("unknown opcode", opcode)
                break outer
            }
        }

        return .halted
    }
}


// Part 1

enum Tile { case scaffold, space, droid, tumble }

let initialProgram = input.components(separatedBy: ",").compactMap { Int($0) }
var vm = VM(program: initialProgram)

var grid: [[Tile]] = [], row: [Tile] = []
var droidRow = 0, droidCol = 0
var s = ""
while case let .output(ascii) = vm.execute() {
    let character = String(UnicodeScalar(ascii)!)
    s += character
    switch character {
    case "#":
        row.append(.scaffold)
    case ".":
        row.append(.space)
    case "^", "V", "<", ">":
        row.append(.droid)
        droidRow = grid.count
        droidCol = row.count - 1
    case "X":
        row.append(.tumble)
    case "\n":
        grid.append(row)
        row = []
    default:
        print("unexpected character", character)
        break
    }
}
print(s)
_ = grid.removeLast()

var intersectionSum = 0
for row in 1..<(grid.count - 1) {
    for col in 1..<(grid[0].count - 1) {
        if grid[row][col] == .scaffold && grid[row + 1][col] == .scaffold && grid[row - 1][col] == .scaffold && grid[row][col + 1] == .scaffold && grid[row][col - 1] == .scaffold {
            print(col, row, col * row)
            intersectionSum += col * row
        }
    }
}
print(intersectionSum)


// Part 2

print("--------------")

enum Direction {
    case up, down, left, right

    var turnedRight: Direction {
        switch self {
        case .up:
            return .right
        case .right:
            return .down
        case .down:
            return .left
        case .left:
            return .up
        }
    }

    var turnedLeft: Direction {
        switch self {
        case .up:
            return .left
        case .left:
            return .down
        case .down:
            return .right
        case .right:
            return .up
        }
    }

    func newPosition(fromRow row: Int, col: Int) -> (Int, Int) {
        switch self {
        case .up:
            return (row - 1, col)
        case .down:
            return (row + 1, col)
        case .left:
            return (row, col - 1)
        case .right:
            return (row, col + 1)
        }
    }
}

enum Instruction: CustomStringConvertible, Equatable, Hashable {
    case left, right, move(Int), a, b, c

    var stringLength: Int {
        switch self {
        case .left, .right, .a, .b, .c:
            return 1
        case let .move(amount):
            return (amount > 9 ? 2 : 1) // lmao only 1 or 2 digit numbers allowed
        }
    }

    var description: String {
        switch self {
        case .left:
            return "L"
        case .right:
            return "R"
        case let .move(amount):
            return String(amount)
        case .a:
            return "A"
        case .b:
            return "B"
        case .c:
            return "C"
        }
    }

    var intcodeRepresentation: [Int] {
        switch self {
        case .left:
            return [76]
        case .right:
            return [82]
        case .a:
            return [65]
        case .b:
            return [66]
        case .c:
            return [67]
        case let .move(amount):
            // lmao still only 1 or 2 digit numbers allowed
            if amount > 9 {
                return [48 + amount / 10, 48 + amount % 10]
            } else {
                return [48 + amount]
            }
        }
    }
}

func stringLength(ofPath path: [Instruction]) -> Int {
    return path.count - 1 + path.reduce(0) { $0 + $1.stringLength }
}

func replace(subpath: [Instruction], inPath path: [Instruction], with replacement: Instruction) -> ([Instruction], Int) {
    var path = path
    var i = 0, replacements = 0
    while i < (path.count - subpath.count + 1) {
        let subrange = i..<(i + subpath.count)
        if subpath == Array(path[subrange]) {
            path.replaceSubrange(subrange, with: [replacement])
            replacements += 1
        }
        i += 1
    }
    return (path, replacements)
}


var direction = Direction.up
var path: [Instruction] = [], forwardMovements = 0
while true {
    let candidateMoves = [
        (direction, direction.newPosition(fromRow: droidRow, col: droidCol)),
        (direction.turnedRight, direction.turnedRight.newPosition(fromRow: droidRow, col: droidCol)),
        (direction.turnedLeft, direction.turnedLeft.newPosition(fromRow: droidRow, col: droidCol))
    ]

    var moved = false
    for (i, (newDirection, (newDroidRow, newDroidCol))) in candidateMoves.enumerated() {
        if newDroidRow < 0 || newDroidRow >= grid.count || newDroidCol < 0 || newDroidCol >= grid[0].count {
            continue
        } else if grid[newDroidRow][newDroidCol] == .space {
            continue
        }
        direction = newDirection
        droidRow = newDroidRow
        droidCol = newDroidCol
        moved = true

        if i == 0 {
            forwardMovements += 1
        } else if i == 1 {
            path += [.move(forwardMovements), .right]
            forwardMovements = 1
        } else if i == 2 {
            path += [.move(forwardMovements), .left]
            forwardMovements = 1
        }
        break
    }

    if !moved {
        break
    }
}
path.append(.move(forwardMovements))
if case .move(0) = path[0] {
    _ = path.removeFirst()
}
print(path)


var subpathsSet = Set<[Instruction]>()
for a in 0..<(path.count - 2) {
    for b in (a + 2)..<path.count {
        let subpath = Array(path[a..<b])
        let length = stringLength(ofPath: subpath)
        if length <= 20 && replace(subpath: subpath, inPath: path, with: .a).1 > 1 {
            subpathsSet.insert(subpath)
        }
    }
}
let subpaths = Array(subpathsSet).sorted { stringLength(ofPath: $0) > stringLength(ofPath: $1) }

var finalMain: [Instruction] = [], finalA: [Instruction] = [], finalB: [Instruction] = [], finalC: [Instruction] = []
outer: for a in 0..<(subpaths.count - 2) {
    let (substitutedPathA, _) = replace(subpath: subpaths[a], inPath: path, with: .a)
    for b in (a + 1)..<(subpaths.count - 1) {
        let (substitutedPathB, bReplacements) = replace(subpath: subpaths[b], inPath: substitutedPathA, with: .b)
        if bReplacements < 2 {
            continue
        }
        for c in (b + 1)..<subpaths.count {
            let (substitutedPathC, _) = replace(subpath: subpaths[c], inPath: substitutedPathB, with: .c)
            if (substitutedPathC.filter { $0 != .a && $0 != .b && $0 != .c }).count == 0 && stringLength(ofPath: substitutedPathC) <= 20 {
                print("main", substitutedPathC)
                print("a", subpaths[a])
                print("b", subpaths[b])
                print("c", subpaths[c])
                finalA = subpaths[a]
                finalB = subpaths[b]
                finalC = subpaths[c]
                finalMain = substitutedPathC
                break outer
            }
        }
    }
}


let programInput = [finalMain, finalA, finalB, finalC, []].map { $0.map { $0.intcodeRepresentation }.joined(separator: [44]) }.joined(separator: [10])

vm = VM(program: initialProgram)
vm.program[0] = 2
vm.inputs = Array(programInput + [110, 10])
s = ""
var dust = 0
while case let .output(value) = vm.execute() {
    if value > 127 {
        dust = value
        continue
    }
    let character = String(UnicodeScalar(value)!)
    s += character
}
print(s)
print(dust)
