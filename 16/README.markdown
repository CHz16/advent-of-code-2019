Day 16: Flawed Frequency Transmission
=====================================

https://adventofcode.com/2019/day/16

So the transformation here is patterned nicely enough that I feel there must be a very clever way to do these quickly, but instead I just threw my new computer at it lmao oops

Part 1 is just implemented exactly as specified, nothing worth commenting on there.

Part 2 was solved with the observation that by the process definition, after a round, any element in the second half of the signal will be just the sum of every element from that to the end. So we can just brute force this by creating a truncated signal array that starts at the offset and goes to the end and then just accumulate sums to do the transformation. O(n) for each round!

The way I use to create the signal array in part 2 is not particularly efficient, since it creates the whole signal * 10000 array and then truncates it from the offset; better would be to just create the ending of the array and nothing else. I didn't do that though because it was faster to write it this way~

UPDATE: Added a `16-2.swift` with a "better" approach to part 2 that's slightly slower than my original solution for the puzzle parameters but performs way, way better when you increase the number of rounds; with some informal timing, going from 100 rounds to 1 billion changed the runtime from about 24 seconds to only about 35 seconds, which seems good.

The key observation I had was to reframe "after a round, any element in the second half of the signal will be just the sum of every element from that to the end" to "after any round, any element in the second half of the signal will be some linear combination of every element from that to the end." This is powerful because, if we can find a closed-form expression for the coefficient of a given element in a given round, then we can directly calculate that round without having to calculate the previous rounds. After calculating a few rounds for a small signal by hand, I found it's a relatively simple binomial coefficient: given a signal digit *i_0*, the value of that digit in round *r* will be *n_0* * *i_0* + *n_1* * *i_1* + …, where *n_k* = (*r* + *k* - 1) nCr *k*. And naturally, because of the modular arithmetic of the problem, all we really need is *n_k* mod 10.

However, this introduces a new problem: binomial coefficients of the sizes we're dealing with in this problem very quickly get out of control, and can take ages to calculate recursively even with caching. My initial implementation of this was much slower than my original part 2 solution. Luckily, number theory can save us: there's a very efficient way to calculate a binomial coefficient modulo a prime called [Lucas's theorem](https://en.wikipedia.org/wiki/Lucas%27s_theorem). There's a slight hitch in that our base, 10, is not prime, but thanks to the [Chinese remainder theorem](https://en.wikipedia.org/wiki/Chinese_remainder_theorem), we can use Lucas's theorem to calculate it modulo 2 and modulo 5 and then combine the results to find it modulo 10.

UPDATE 2: I just cut off another second or two from the runtime of `16-2.swift` just by removing an upfront cost; I realized that with the new algorithm, I didn't actually need to construct the long array of the full signal any more.
