#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "16.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

func flawedFrequencyTransmission(of signal: [Int], rounds: Int) -> [Int] {
    var signal = signal
    for _ in 0..<rounds {
        var newSignal: [Int] = []
        for i in 1...signal.count {
            var digit = 0
            for (position, initialDigit) in signal.enumerated() {
                if (position + 1) % (i * 4) >= i && (position + 1) % (i * 4) < 2 * i {
                    digit += initialDigit
                } else if (position + 1) % (i * 4) >= 3 * i {
                    digit -= initialDigit
                }
            }
            newSignal.append(abs(digit) % 10)
        }
        signal = newSignal
    }
    return signal
}

let signal = input.compactMap { Int(String($0)) }
print(flawedFrequencyTransmission(of: signal, rounds: 100)[0..<8])


// Part 2

func decompose(_ n: Int, inBase base: Int) -> [Int] {
    var n = n
    var expansion: [Int] = []
    while n > 0 {
        expansion.insert(n % base, at: 0)
        n /= base
    }
    return expansion
}

var binomialCoefficients: [Int: [Int: Int]] = [:]
func binomialCoefficient(n: Int, k: Int) -> Int {
    if k == 0 || k == n {
        return 1
    } else if n < k {
        return 0
    } else if let bc = binomialCoefficients[n]?[k] {
        return bc
    }

    let bc = (binomialCoefficient(n: n - 1, k: k - 1) + binomialCoefficient(n: n - 1, k: k)) % 10
    binomialCoefficients[n, default: [:]][k] = bc
    return bc
}

func binomialCoefficientModPrime(n: Int, k: Int, p: Int) -> Int {
    var nBaseP: [Int] = decompose(n, inBase: p)
    var kBaseP: [Int] = decompose(k, inBase: p)

    if nBaseP.count < kBaseP.count {
        nBaseP = repeatElement(0, count: kBaseP.count - nBaseP.count) + nBaseP
    } else if kBaseP.count < nBaseP.count {
        kBaseP = repeatElement(0, count: nBaseP.count - kBaseP.count) + kBaseP
    }

    var product = 1
    for (ni, ki) in zip(nBaseP, kBaseP) {
        product = (product * binomialCoefficient(n: ni, k: ki)) % p
        if product == 0 {
            break
        }
    }
    return product
}

let modulo10ForModulos2And5 = [
    [0, 6, 2, 8, 4],
    [5, 1, 7, 3, 9]
]
var binomialCoefficientsMod10: [Int: [Int: Int]] = [:]
func binomialCoefficientMod10(n: Int, k: Int) -> Int {
    if let bc = binomialCoefficientsMod10[n]?[k] {
        return bc
    }

    let binomialCoefficientMod2 = binomialCoefficientModPrime(n: n, k: k, p: 2)
    let binomialCoefficientMod5 = binomialCoefficientModPrime(n: n, k: k, p: 5)
    let bc = modulo10ForModulos2And5[binomialCoefficientMod2][binomialCoefficientMod5]
    binomialCoefficientsMod10[n, default: [:]][k] = bc
    return bc
}

print("--------------")

let offset = signal[0..<7].reduce(0) { 10 * $0 + $1 }
var output: [Int] = []
for i in 0..<8 {
    var newDigit = signal[(offset + i) % signal.count]
    for n in 0..<(signal.count * 10000 - offset - i - 1) {
        let digit = signal[(offset + i + 1 + n) % signal.count]
        newDigit = (newDigit + binomialCoefficientMod10(n: 100 + n, k: n + 1) * digit) % 10
    }
    output.append(abs(newDigit))
}
print(output)
