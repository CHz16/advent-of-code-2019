#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "16.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

func flawedFrequencyTransmission(of signal: [Int], rounds: Int) -> [Int] {
    var signal = signal
    for _ in 0..<rounds {
        var newSignal: [Int] = []
        for i in 1...signal.count {
            var digit = 0
            for (position, initialDigit) in signal.enumerated() {
                if (position + 1) % (i * 4) >= i && (position + 1) % (i * 4) < 2 * i {
                    digit += initialDigit
                } else if (position + 1) % (i * 4) >= 3 * i {
                    digit -= initialDigit
                }
            }
            newSignal.append(abs(digit) % 10)
        }
        signal = newSignal
    }
    return signal
}

let signal = input.compactMap { Int(String($0)) }
print(flawedFrequencyTransmission(of: signal, rounds: 100)[0..<8])


// Part 2

print("--------------")

let offset = signal[0..<7].reduce(0) { 10 * $0 + $1 }
var newSignal = Array(repeating: signal, count: 10000).flatMap { $0 }
newSignal = Array(newSignal[offset...])

for _ in 0..<100 {
    var sum = 0
    for i in (0..<newSignal.count).reversed() {
        sum = (sum + newSignal[i]) % 10
        newSignal[i] = sum
    }
}
print(newSignal[0..<8])
