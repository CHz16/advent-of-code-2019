Day 13: Care Package
====================

https://adventofcode.com/2019/day/13

Another puzzle where the first part is just to set up a simple Intcode VM input/output loop, and the second part is to get fancier. Since there's exactly one ball and paddle tile in the game, I was worried that predictive movement of the paddle would be required to avoid lagging behind the ball, but I tried just the simple inputs of "move the paddle toward the ball if it's not directly under it" and that worked completely fine for my input. So that was nice.
