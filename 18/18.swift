#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "18-2.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1 or 2

struct Point: Hashable {
    let row, col: Int

    var adjacents: [Point] {
        return [
            Point(row: row + 1, col: col),
            Point(row: row - 1, col: col),
            Point(row: row, col: col + 1),
            Point(row: row, col: col - 1)
        ]
    }
}
enum Tile: Equatable { case wall, empty, door(String), key(String) }

var vault: [[Tile]] = []
var keyLocations: [String: Point] = [:]
var startLocations: [Point] = []
input.enumerateLines { (line, stop) in
    var row: [Tile] = []
    for character in line {
        if character == "#" {
            row.append(.wall)
        } else if character == "." {
            row.append(.empty)
        } else if character == "@" {
            startLocations.append(Point(row: vault.count, col: row.count))
            row.append(.empty)
        } else if character.isUppercase {
            row.append(.door(character.lowercased()))
        } else {
            keyLocations[String(character)] = Point(row: vault.count, col: row.count)
            row.append(.key(String(character)))
        }
    }
    vault.append(row)
}

struct PathState: Hashable { let points: [Point]; let remainingKeys: [String] }
typealias Path = (path: [String], distance: Int)
var shortestPaths: [PathState: Path] = [:]
func findShortestPath(fromPoints points: [Point], toKeys remainingKeys: [String], currentPath: [String]) -> Path {
    let currentPathState = PathState(points: points, remainingKeys: remainingKeys)
    if let shortestPath = shortestPaths[currentPathState] {
        return shortestPath
    }
    if remainingKeys.isEmpty {
        return Path(path: currentPath, distance: 0)
    }

    var visited: [Point: Bool] = [:]
    var queue: [(robotNumber: Int, point: Point, distance: Int)] = []
    for (robotNumber, point) in points.enumerated() {
        visited[point] = true
        queue.append((robotNumber: robotNumber, point: point, distance: 0))
    }

    var reachableKeys: [(robotNumber: Int, key: String, distance: Int)] = []
    bfs: while !queue.isEmpty {
        let (robotNumber, location, distance) = queue.removeFirst()
        visited[location] = true

        if case let .key(foundKey) = vault[location.row][location.col] {
            if remainingKeys.contains(foundKey) {
                reachableKeys.append((robotNumber: robotNumber, key: foundKey, distance: distance))
                continue bfs
            }
        }

        for adjacent in location.adjacents {
            if visited[adjacent] != nil {
                continue
            }
            let cell = vault[adjacent.row][adjacent.col]
            if cell == .wall {
                continue
            }
            if case let .door(foundDoor) = cell {
                if remainingKeys.contains(foundDoor) {
                    continue
                }
            }
            queue.append((robotNumber: robotNumber, point: adjacent, distance: distance + 1))
        }
    }

    var candidateShortestPaths: [Path] = []
    for (robotNumber, key, distance) in reachableKeys {
        var newRobotLocations = points
        newRobotLocations[robotNumber] = keyLocations[key]!
        let shortestRemainderPath = findShortestPath(fromPoints: newRobotLocations, toKeys: remainingKeys.filter { $0 != key }, currentPath: currentPath + [key])
        candidateShortestPaths.append(Path(path: shortestRemainderPath.path, distance: distance + shortestRemainderPath.distance))
    }
    let shortestPath = candidateShortestPaths.min { $0.distance < $1.distance }!
    shortestPaths[currentPathState] = shortestPath
    return shortestPath
}
print(findShortestPath(fromPoints: startLocations, toKeys: Array(keyLocations.keys), currentPath: ["start"]))
