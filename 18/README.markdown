Day 18: Many-Worlds Interpretation
==================================

https://adventofcode.com/2019/day/18

Ugh, this feels like by far the hardest year of AoC to date. What is this mess lmao

I wasted a whole bunch of time on my first approach, which was throwing A* at it and hoping it would stop executing at some point. But it never did for the fourth sample input, let alone the problem input, so that owned

So I was like "well now what am I going to do," and I stared at it for a while and realized it has a subproblem structure I could try doing something with. Specifically, the shortest path from a point to keys [A, B, C, …] is going to the be the shortest path of [the shortest path to key A + the shortest path to the remaining keys, the shortest path to key B + the shortest path to the remaining keys, …].

We can find the shortest paths from the point to all reachable keys pretty straightforwardly with a BFS, taking open doors into account. This theoretically does have to be done in every recursive step and can't be done up front, because opening doors in the course of traversing the maze could create additional paths to keys. The sample inputs and my puzzle inputs seem to be structured so this will never happen, though, but I was playing it safe.

I have the BFS halt a path that lands on a key instead of continuing on, meaning that if the shortest path to key B passes through key A, the BFS will either find an incorrect longer path to B or no path at all. This was a spur-of-the-moment "optimization" because I didn't want to deal with having to handle picking up multiple keys in one path segment. I don't know how this positively or negatively affects runtime, but it shouldn't ever cause the program to produce an incorrect answer, because if the shortest path is really to go to A and then B, then it'll find that path when we search for (the shortest path to key A + the shortest path to the remaining keys).

With the BFS piece, we can take on the higher-level algorithm, which is just bog standard dynamic programming. For each key the BFS found, we recurse and search for the shortest path from that key's location to the other keys we haven't found yet, and the shortest path through all keys starting with that key will be (that key + the path we found in recursion), and the length will be (the distance to that key + the length of the path we found in recursion). We then pick the shortest path out of that last and return it.

The key bit here is that we can cache the shortest path we find for a given starting point and the list of remaining keys; no matter what path we've taken to collect all the other keys, this path will always be the same because the other doors are already open. This is the trick that makes this program actually stop executing.

For part 2, the algorithm broadly still works, but I had to modify the code to handle a list of robots instead of just a single robot. The same code can be run on both the single-robot version of the maze (`18-1.txt`) and the four-robot version (`18-2.txt`).

The BFS changes from finding all keys reachable from a point to finding all keys reachable from a list of points, additionally keeping track of which point reached it. The top-level algorithm then needs to update the list of robots with the new position of the one that moved specifically before passing that down into the recursion.
