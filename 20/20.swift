#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "20.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

// Just directly read in the entire maze data first
enum Tile: Equatable { case void, floor, wall, warp(destinationX: Int, destinationY: Int, recurse: Bool), start, finish, label(String) }
var maze: [[Tile]] = []
input.enumerateLines { (line, stop) in
    var row: [Tile] = []
    for character in line {
        if character == " " {
            row.append(.void)
        } else if character == "#" {
            row.append(.wall)
        } else if character == "." {
            row.append(.floor)
        } else {
            row.append(.label(String(character)))
        }
    }
    maze.append(row)
}

// Parse the warp labels
var labels: [String: [(x: Int, y: Int, recurse: Bool)]] = [:]
for y in 0..<maze.count {
    for x in 0..<maze[0].count {
        if case let .label(character) = maze[y][x] {
            if y > 0 && maze[y - 1][x] == .floor {
                // Label is below maze point
                guard case let .label(secondCharacter) = maze[y + 1][x] else { continue }
                let recurse = (y < maze.count - 2)
                labels[character + secondCharacter, default: []].append((x: x, y: y - 1, recurse: recurse))
                maze[y][x] = .void
                maze[y + 1][x] = .void
            } else if y < maze.count - 2 && maze[y + 2][x] == .floor {
                // Label is above maze point
                guard case let .label(secondCharacter) = maze[y + 1][x] else { continue }
                let recurse = (y > 0)
                labels[character + secondCharacter, default: []].append((x: x, y: y + 2, recurse: recurse))
                maze[y][x] = .void
                maze[y + 1][x] = .void
            } else if x > 0 && maze[y][x - 1] == .floor {
                // Label is right of maze point
                guard case let .label(secondCharacter) = maze[y][x + 1] else { continue }
                let recurse = (x < maze[0].count - 2)
                labels[character + secondCharacter, default: []].append((x: x - 1, y: y, recurse: recurse))
                maze[y][x] = .void
                maze[y][x + 1] = .void
            } else if x < maze.count - 2 && maze[y][x + 2] == .floor {
                // Label is left of maze point
                guard case let .label(secondCharacter) = maze[y][x + 1] else { continue }
                let recurse = (x > 0)
                labels[character + secondCharacter, default: []].append((x: x + 2, y: y, recurse: recurse))
                maze[y][x] = .void
                maze[y][x + 1] = .void
            }
        }
    }
}

// Place the start and finish
maze[labels["AA"]![0].y][labels["AA"]![0].x] = .start
maze[labels["ZZ"]![0].y][labels["ZZ"]![0].x] = .finish

// Create the warps
for (label, locations) in labels {
    // Skip start & finish
    if label == "AA" || label == "ZZ" {
        continue
    }
    maze[locations[0].y][locations[0].x] = .warp(destinationX: locations[1].x, destinationY: locations[1].y, recurse: locations[0].recurse)
    maze[locations[1].y][locations[1].x] = .warp(destinationX: locations[0].x, destinationY: locations[0].y, recurse: locations[1].recurse)
}

// BFS
var queue = [(x: labels["AA"]![0].x, y: labels["AA"]![0].y, distance: 0)]
var visited = Array(repeating: Array(repeating: false, count: maze[0].count), count: maze.count)
while !queue.isEmpty {
    let (x, y, distance) = queue.removeFirst()
    if visited[y][x] {
        continue
    } else if maze[y][x] == .finish {
        print(distance)
        break
    }

    visited[y][x] = true
    let adjacents = [
        (x: x + 1, y: y, distance: distance + 1),
        (x: x - 1, y: y, distance: distance + 1),
        (x: x, y: y + 1, distance: distance + 1),
        (x: x, y: y - 1, distance: distance + 1)
        ].filter { maze[$0.y][$0.x] != .wall && maze[$0.y][$0.x] != .void }
    queue += adjacents

    if case let .warp(destinationX, destinationY, _) = maze[y][x] {
        queue.append((x: destinationX, y: destinationY, distance: distance + 1))
    }
}


// Part 2

print("--------------")

// Literally just the same BFS but also keeping track of depth
var recursiveQueue = [(x: labels["AA"]![0].x, y: labels["AA"]![0].y, distance: 0, depth: 0)]
var recursiveVisited = Array(repeating: Array(repeating: Dictionary<Int, Bool>(), count: maze[0].count), count: maze.count)
while !recursiveQueue.isEmpty {
    let (x, y, distance, depth) = recursiveQueue.removeFirst()
    if recursiveVisited[y][x][depth] != nil {
        continue
    } else if maze[y][x] == .finish && depth == 0 {
        print(distance)
        break
    }

    recursiveVisited[y][x][depth] = true
    let adjacents = [
        (x: x + 1, y: y, distance: distance + 1, depth: depth),
        (x: x - 1, y: y, distance: distance + 1, depth: depth),
        (x: x, y: y + 1, distance: distance + 1, depth: depth),
        (x: x, y: y - 1, distance: distance + 1, depth: depth)
        ].filter { maze[$0.y][$0.x] != .wall && maze[$0.y][$0.x] != .void }
    recursiveQueue += adjacents

    if case let .warp(destinationX, destinationY, recurse) = maze[y][x] {
        // You can't step out from layer 0, allowing it can produce wrong answers
        if recurse || (depth > 0 && !recurse) {
            recursiveQueue.append((x: destinationX, y: destinationY, distance: distance + 1, depth: depth + (recurse ? 1 : -1)))
        }
    }
}
