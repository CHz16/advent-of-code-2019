Day 20: Donut Maze
==================

https://adventofcode.com/2019/day/20

Having started this one late, I decided to waste some time and actually parse the label data directly from the puzzle data instead of preprocessing it to make it easier. Enjoy it while you can!

Part 1 is a simple BFS through adjacent squares that are open, with the addition that if you're on a warp square, you also add the warp exit to the queue as well.

Part 2 is, well, it's exactly the same BFS as part 1, except you also need to keep track of what layer you're on inside a search state. The only real gotcha is that you can't step outward from layer 0, which is only a gotcha if you don't read the puzzle description completely (haha who'd do that certainly not me). Theoretically there could be an enormous gross branch factor with the potentially infinite depth of the search space, but I just threw the BFS directly at it first before trying to think of a good pruning method and it worked just fine. Hooray for computers with big CPUs in them!
