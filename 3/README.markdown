Day 3: Crossed Wires
====================

https://adventofcode.com/2019/day/3

Since I did this one late, I decided to overengineer the solution: instead of plotting the wires or something, instead I keep track of the line segments and check if/where they intersect, along with the total traversed distance along each wire for part 2. I did the intersection calculations by considering the line segments to be rectangles, because I know how to do that easily.
