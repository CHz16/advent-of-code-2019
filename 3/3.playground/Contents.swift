//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "3", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

typealias Point = (Int, Int)

struct LineSegment {
    let startPoint, endPoint: Point
    let upperLeftX, upperLeftY, lowerRightX, lowerRightY: Int

    init(_ point1: Point, _ point2: Point) {
        startPoint = point1
        endPoint = point2

        upperLeftX = min(point1.0, point2.0)
        upperLeftY = max(point1.1, point2.1)
        lowerRightX = max(point1.0, point2.0)
        lowerRightY = min(point1.1, point2.1)
    }

    var length: Int {
        return (lowerRightX - upperLeftX) + (upperLeftY - lowerRightY)
    }

    func intersects(_ lineSegment: LineSegment) -> Bool {
        if lowerRightX < lineSegment.upperLeftX || lineSegment.lowerRightX < upperLeftX {
            return false
        }
        if lowerRightY > lineSegment.upperLeftY || lineSegment.lowerRightY > upperLeftY {
            return false
        }
        return true
    }
}

func makeLineSegments(fromDefinition definition: [String]) -> [LineSegment] {
    var lineSegments: [LineSegment] = []

    var currentPoint: Point = (0, 0)
    for movement in definition {
        let direction = movement.prefix(1), distance = Int(movement.suffix(from: movement.index(after: movement.startIndex)))!

        let newPoint: Point
        switch direction {
        case "U":
            newPoint = (currentPoint.0, currentPoint.1 + distance)
        case "D":
            newPoint = (currentPoint.0, currentPoint.1 - distance)
        case "L":
            newPoint = (currentPoint.0 - distance, currentPoint.1)
        case "R":
            newPoint = (currentPoint.0 + distance, currentPoint.1)
        default:
            // ideally never called lmao
            newPoint = (0, 0)
        }

        lineSegments.append(LineSegment(currentPoint, newPoint))
        currentPoint = newPoint
    }

    return lineSegments
}

let lines = input.components(separatedBy: .newlines)
let wire1 = makeLineSegments(fromDefinition: lines[0].components(separatedBy: ","))
let wire2 = makeLineSegments(fromDefinition: lines[1].components(separatedBy: ","))

var intersections: [(distanceFromCenter: Int, stepsFromCenter: Int)] = []
var wire2Steps = 0
for (segment2Index, segment2) in wire2.enumerated() {
    var wire1Steps = 0
    for (segment1Index, segment1) in wire1.enumerated() {
        if segment1Index == 0 && segment2Index == 0 {
            continue
        }

        if segment2.intersects(segment1) {
            let intersectionX, intersectionY: Int
            if segment1.upperLeftX == segment1.lowerRightX {
                intersectionX = segment1.upperLeftX
                intersectionY = segment2.upperLeftY
            } else {
                intersectionX = segment2.upperLeftX
                intersectionY = segment1.upperLeftY
            }

            let intersectionPoint = Point(intersectionX, intersectionY)
            let segment1Fragment = LineSegment(segment1.startPoint, intersectionPoint)
            let segment2Fragment = LineSegment(segment2.startPoint, intersectionPoint)

            intersections.append((distanceFromCenter: abs(intersectionX) + abs(intersectionY), stepsFromCenter: wire1Steps + wire2Steps + segment1Fragment.length + segment2Fragment.length))
        }

        wire1Steps += segment1.length
    }
    wire2Steps += segment2.length
}

print(intersections.min { $0.distanceFromCenter < $1.distanceFromCenter }!.distanceFromCenter)

// Part 2

print("--------------")

print(intersections.min { $0.stepsFromCenter < $1.stepsFromCenter }!.stepsFromCenter)

