#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "23.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


struct VM {
    enum State: Equatable { case output(Int), waitingForInput, halted, running }

    var program: [Int]
    var inputs: [Int] = []

    var pc = 0
    var relativeBase = 0


    mutating func extendMemory(toLocation memoryLocation: Int) {
        if memoryLocation >= program.count {
            program += Array(repeating: 0, count: (memoryLocation - program.count + 1))
        }
    }

    mutating func write(value: Int, toMemoryLocation memoryLocation: Int) {
        extendMemory(toLocation: memoryLocation)
        program[memoryLocation] = value
    }

    mutating func getParameter(atIndex index: Int, withMode mode: Int) -> Int {
        if mode == 1 {
            // mode 1: immediate
            return program[index]
        }

        // mode 0: position
        // mode 2: relative
        let memoryLocation = (mode == 0) ? program[index] : (program[index] + relativeBase)
        extendMemory(toLocation: memoryLocation)
        return program[memoryLocation]
    }

    func getWriteLocation(atIndex index: Int, withMode mode: Int) -> Int {
        if mode == 0 {
            // mode 0: position
            return program[index]
        } else {
            // mode 2: relative
            return program[index] + relativeBase
        }
    }

    mutating func execute() -> State {
        while true {
            let result = step()
            if result == .running {
                continue
            }
            return result
        }
    }

    mutating func step() -> State {
        if pc < 0 || pc >= program.count {
            return .halted
        }

        let opcode = program[pc] % 100
        let parameter1Mode, parameter2Mode, parameter3Mode: Int
        if program[pc] > 100 {
            parameter1Mode = (program[pc] / 100) % 10
        } else {
            parameter1Mode = 0
        }
        if program[pc] > 1000 {
            parameter2Mode = (program[pc] / 1000) % 10
        } else {
            parameter2Mode = 0
        }
        if program[pc] > 10000 {
            parameter3Mode = (program[pc] / 10000) % 10
        } else {
            parameter3Mode = 0
        }

        switch opcode {
        case 1:
            // +
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
            write(value: operand1 + operand2, toMemoryLocation: location)
            pc += 4
        case 2:
            // *
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
            write(value: operand1 * operand2, toMemoryLocation: location)
            pc += 4
        case 3:
            // input
            if inputs.isEmpty {
                return .waitingForInput
            } else {
                let input = inputs.removeFirst()
                let location = getWriteLocation(atIndex: pc + 1, withMode: parameter1Mode)
                write(value: input, toMemoryLocation: location)
                pc += 2
            }
        case 4:
            // output
            let output = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            pc += 2
            return .output(output)
        case 5:
            // jump if true
            let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            if condition != 0 {
                pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            } else {
                pc += 3
            }
        case 6:
            // jump if false
            let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            if condition == 0 {
                pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            } else {
                pc += 3
            }
        case 7:
            // less than
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
            write(value: (operand1 < operand2) ? 1 : 0, toMemoryLocation: location)
            pc += 4
        case 8:
            // equals
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
            write(value: (operand1 == operand2) ? 1 : 0, toMemoryLocation: location)
            pc += 4
        case 9:
            // update relative base
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            relativeBase += operand1
            pc += 2
        case 99:
            // halt
            return .halted
        default:
            // oh no
            print("unknown opcode", opcode)
            return .halted
        }

        return .running
    }

}


// Part 1

struct Packet {
    let from: Int
    var x, y: Int?

    var membersConsumed = 0
    var hasData: Bool { return (x != nil) }
    var isFull: Bool { return (y != nil) }
    var isConsumed: Bool { return (membersConsumed == 2) }

    mutating func append(_ value: Int) {
        if x == nil {
            x = value
        } else {
            y = value
        }
    }

    mutating func consume() -> Int {
        membersConsumed += 1
        if membersConsumed == 1 {
            return x!
        } else {
            return y!
        }
    }
}

let initialProgram = input.components(separatedBy: ",").compactMap { Int($0) }

var computers: [VM] = []
for i in 0..<50 {
    var vm = VM(program: initialProgram)
    vm.inputs = [i]
    computers.append(vm)
}

var outputQueue: [[Packet]] = Array(repeating: [], count: 256)
var bufferedOutputs: [Int?] = Array(repeating: nil, count: computers.count)
var sendingTo: [Int?] = Array(repeating: nil, count: computers.count)
while true {
    for i in 0..<computers.count {
        let result = computers[i].step()
        if result == .waitingForInput {
            if outputQueue[i].isEmpty || !outputQueue[i][0].isFull {
                computers[i].inputs = [-1]
            } else {
                computers[i].inputs = [outputQueue[i][0].consume()]
                if outputQueue[i][0].isConsumed {
                    _ = outputQueue[i].removeFirst()
                }
            }
            _ = computers[i].step()
        } else if case let .output(value) = result {
            bufferedOutputs[i] = value
        }
    }

    for i in 0..<computers.count {
        if let value = bufferedOutputs[i] {
            if let destination = sendingTo[i] {
                let packetIndex = outputQueue[destination].firstIndex { $0.from == i && !$0.isFull }!
                outputQueue[destination][packetIndex].append(value)
                if outputQueue[destination][packetIndex].isFull {
                    sendingTo[i] = nil
                }
            } else {
                outputQueue[value].append(Packet(from: i))
                sendingTo[i] = value
            }
            bufferedOutputs[i] = nil
        }
    }

    if !outputQueue[255].isEmpty && outputQueue[255][0].isFull {
        print(outputQueue[255][0])
        break
    }
}


// Part 2

print("--------------")

var lastNatYValue = Int.min
var idle: [Bool] = Array(repeating: false, count: computers.count)
while true {
    for i in 0..<computers.count {
        let result = computers[i].step()
        if result == .waitingForInput {
            if outputQueue[i].isEmpty || !outputQueue[i][0].isFull {
                idle[i] = true
                computers[i].inputs = [-1]
            } else {
                idle[i] = false
                computers[i].inputs = [outputQueue[i][0].consume()]
                if outputQueue[i][0].isConsumed {
                    _ = outputQueue[i].removeFirst()
                }
            }
            _ = computers[i].step()
        } else if case let .output(value) = result {
            bufferedOutputs[i] = value
        }
    }

    for i in 0..<computers.count {
        if let value = bufferedOutputs[i] {
            if let destination = sendingTo[i] {
                let packetIndex = outputQueue[destination].firstIndex { $0.from == i && !$0.isFull }!
                outputQueue[destination][packetIndex].append(value)
                if outputQueue[destination][packetIndex].isFull {
                    sendingTo[i] = nil
                }
            } else {
                outputQueue[value].append(Packet(from: i))
                sendingTo[i] = value
            }
            bufferedOutputs[i] = nil
        }
    }

    if idle.allSatisfy({ $0 }) {
        let natPacket = outputQueue[255].last { $0.isFull }!
        if natPacket.y == lastNatYValue {
            print(lastNatYValue)
            break
        }

        print("sending to 0", natPacket)
        outputQueue[0] = [natPacket]
        outputQueue[255] = []
        idle[0] = false
        lastNatYValue = natPacket.y!
    }
}
