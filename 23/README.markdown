Day 23: Category Six
====================

https://adventofcode.com/2019/day/23

Concurrency, oh boy!

I wasn't sure how strict the parallel execution of each VM has to be, whether it would be safe to actually execute each machine until they want an input or output and then distribute values then. So I went completely overboard and have each machine step one instruction each cycle, then any outputs made during that cycle are collected and distributed for the next cycle. This is actually mildly tricky because a packet consist of three output values, so the manager waits until a packet is complete before making it visible.

Part 2 is just the exact same code copy-pasted, except with an idle check (a flag is set for idle when a machine requests an input and gets nothing, and unset if it does receive a value).
