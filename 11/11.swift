#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "11.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


struct VM {
    enum State { case output(Int), waitingForInput, halted }

    var program: [Int]
    var inputs: [Int] = []

    var pc = 0
    var relativeBase = 0


    mutating func extendMemory(toLocation memoryLocation: Int) {
        if memoryLocation >= program.count {
            program += Array(repeating: 0, count: (memoryLocation - program.count + 1))
        }
    }

    mutating func write(value: Int, toMemoryLocation memoryLocation: Int) {
        extendMemory(toLocation: memoryLocation)
        program[memoryLocation] = value
    }

    mutating func getParameter(atIndex index: Int, withMode mode: Int) -> Int {
        if mode == 1 {
            // mode 1: immediate
            return program[index]
        }

        // mode 0: position
        // mode 2: relative
        let memoryLocation = (mode == 0) ? program[index] : (program[index] + relativeBase)
        extendMemory(toLocation: memoryLocation)
        return program[memoryLocation]
    }

    func getWriteLocation(atIndex index: Int, withMode mode: Int) -> Int {
        if mode == 0 {
            // mode 0: position
            return program[index]
        } else {
            // mode 2: relative
            return program[index] + relativeBase
        }
    }

    mutating func execute() -> State {
        outer: while pc < program.count {
            let opcode = program[pc] % 100
            let parameter1Mode, parameter2Mode, parameter3Mode: Int
            if program[pc] > 100 {
                parameter1Mode = (program[pc] / 100) % 10
            } else {
                parameter1Mode = 0
            }
            if program[pc] > 1000 {
                parameter2Mode = (program[pc] / 1000) % 10
            } else {
                parameter2Mode = 0
            }
            if program[pc] > 10000 {
                parameter3Mode = (program[pc] / 10000) % 10
            } else {
                parameter3Mode = 0
            }

            switch opcode {
            case 1:
                // +
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
                write(value: operand1 + operand2, toMemoryLocation: location)
                pc += 4
            case 2:
                // *
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
                write(value: operand1 * operand2, toMemoryLocation: location)
                pc += 4
            case 3:
                // input
                if inputs.isEmpty {
                    return .waitingForInput
                } else {
                    let input = inputs.removeFirst()
                    let location = getWriteLocation(atIndex: pc + 1, withMode: parameter1Mode)
                    write(value: input, toMemoryLocation: location)
                    pc += 2
                }
            case 4:
                // output
                let output = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                pc += 2
                return .output(output)
            case 5:
                // jump if true
                let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                if condition != 0 {
                    pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                } else {
                    pc += 3
                }
            case 6:
                // jump if false
                let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                if condition == 0 {
                    pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                } else {
                    pc += 3
                }
            case 7:
                // less than
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
                write(value: (operand1 < operand2) ? 1 : 0, toMemoryLocation: location)
                pc += 4
            case 8:
                // equals
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
                write(value: (operand1 == operand2) ? 1 : 0, toMemoryLocation: location)
                pc += 4
            case 9:
                // update relative base
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                relativeBase += operand1
                pc += 2
            case 99:
                // halt
                break outer
            default:
                // oh no
                print("unknown opcode", opcode)
                break outer
            }
        }

        return .halted
    }
}


// Part 1

let initialProgram = input.components(separatedBy: ",").compactMap { Int($0) }

enum PaintState { case unpainted, black, white }
enum Direction { case left, right, up, down }

var hull: [Int: [Int: PaintState]] = [:]
var vm = VM(program: initialProgram)
var currentX = 0, currentY = 0, currentDirection = Direction.up
var uniquePaintedTiles = 0
while case .waitingForInput = vm.execute() {
    if hull[currentY] == nil {
        hull[currentY] = [:]
    }
    if hull[currentY]?[currentX] == nil {
        hull[currentY]![currentX] = .unpainted
    }

    vm.inputs = [(hull[currentY]![currentX] == .white) ? 1 : 0]
    if case let .output(value) = vm.execute() {
        if hull[currentY]![currentX] == .unpainted {
            uniquePaintedTiles += 1
        }
        hull[currentY]![currentX] = (value == 0) ? .black : .white
    }
    if case let .output(value) = vm.execute() {
        switch currentDirection {
        case .left:
            currentDirection = (value == 0) ? .down : .up
        case .right:
            currentDirection = (value == 0) ? .up : .down
        case .up:
            currentDirection = (value == 0) ? .left : .right
        case .down:
            currentDirection = (value == 0) ? .right : .left
        }
        switch currentDirection {
        case .left:
            currentX -= 1
        case .right:
            currentX += 1
        case .up:
            currentY -= 1
        case .down:
            currentY += 1
        }
    }
}

print(uniquePaintedTiles)


// Part 2

print("--------------")

hull = [0: [0: .white]]
vm = VM(program: initialProgram)
currentX = 0
currentY = 0
currentDirection = .up
var minX = 0, maxX = 0, minY = 0, maxY = 0
while case .waitingForInput = vm.execute() {
    minX = min(minX, currentX)
    maxX = max(maxX, currentX)
    minY = min(minY, currentY)
    maxY = max(maxY, currentY)

    if hull[currentY] == nil {
        hull[currentY] = [:]
    }
    if hull[currentY]?[currentX] == nil {
        hull[currentY]![currentX] = .unpainted
    }

    vm.inputs = [(hull[currentY]![currentX] == .white) ? 1 : 0]
    if case let .output(value) = vm.execute() {
        hull[currentY]![currentX] = (value == 0) ? .black : .white
    }
    if case let .output(value) = vm.execute() {
        switch currentDirection {
        case .left:
            currentDirection = (value == 0) ? .down : .up
        case .right:
            currentDirection = (value == 0) ? .up : .down
        case .up:
            currentDirection = (value == 0) ? .left : .right
        case .down:
            currentDirection = (value == 0) ? .right : .left
        }
        switch currentDirection {
        case .left:
            currentX -= 1
        case .right:
            currentX += 1
        case .up:
            currentY += 1
        case .down:
            currentY -= 1
        }
    }
}

for y in (minY...maxY).reversed() {
    var row = ""
    for x in minX...maxX {
        row += (hull[y]![x] == .white) ? "*" : " "
    }
    print(row)
}
