Day 11: Space Police
====================

https://adventofcode.com/2019/day/11

Honestly fairly easy compared to the last couple, provided you have a functioning Intcode VM (which I do, evidently). Just need to pass values to and from the VM and update some external state (the hull) accordingly. This code uses my usual bullshit of using a dictionary to represent a 2D grid that might be able to expand to an unknowable extent in all four directions.

* Part 1: 197th place (14:27)
* Part 2: 188th place (20:12)
