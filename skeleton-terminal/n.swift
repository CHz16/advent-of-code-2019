#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "n.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

print(input)


// Part 2

print("--------------")
