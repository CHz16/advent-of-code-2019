//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "2", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var program = input.components(separatedBy: ",").compactMap { Int($0) }

program[1] = 12
program[2] = 2

var pc = 0
while pc < program.count && program[pc] != 99 {
    let opcode = program[pc], operand1 = program[program[pc + 1]], operand2 = program[program[pc + 2]], location = program[pc + 3]
    if opcode == 1 {
        program[location] = operand1 + operand2
    } else if opcode == 2 {
        program[location] = operand1 * operand2
    }
    pc += 4
}
print(program[0])


// Part 2

print("--------------")

outer: for noun in 0...99 {
    for verb in 0...99 {
        var program = input.components(separatedBy: ",").compactMap { Int($0) }
        program[1] = noun
        program[2] = verb

        var pc = 0
        while pc < program.count && program[pc] != 99 {
            let opcode = program[pc], operand1 = program[program[pc + 1]], operand2 = program[program[pc + 2]], location = program[pc + 3]
            if opcode == 1 {
                program[location] = operand1 + operand2
            } else if opcode == 2 {
                program[location] = operand1 * operand2
            }
            pc += 4
        }

        if program[0] == 19690720 {
            print(100 * noun + verb)
            break outer
        }
    }
}
