Day 2: 1202 Program Alarm
=========================

https://adventofcode.com/2019/day/2

Oh boy it's the start of this year's VM sequence of puzzles. This one is just simple math and array manipulation so that's all I did here. I'm sure this slapdash implementation will prove completely insufficient for later puzzles that build on this!

Part 2 there is probably a closed-form way to calculate the final value given the first two inputs, but since this technically could be self-modifying code, I just brute forced it with nested loops because the input ranges aren't large.

* Part 1: 344th place (9:29)
* Part 2: 159th place (12:21)
