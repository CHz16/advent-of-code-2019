Day 14: Space Stoichiometry
===========================

https://adventofcode.com/2019/day/14

My solution works on the assumptions that (1) every product can only be made by one reaction and (2) we need to use each reaction, which are true for all the test data and my puzzle data. It works backwards, keeping a list of all reagents that we want to produce, starting with 1 FUEL. It also maintains a list of how many times each reagent appears on the left hand side of a reaction that we haven't used yet. For example, on the first sample data:

```
10 ORE => 10 A
1 ORE => 1 B
7 A, 1 B => 1 C
7 A, 1 C => 1 D
7 A, 1 D => 1 E
7 A, 1 E => 1 FUEL
```

The list would be `["ORE": 2, "A": 4, "B": 1, "C": 1, "D": 1, "E": 1, "FUEL": 0]`. We can think of this list as the number of times remaining that we have to use each reagent to produce something else. Any entry in this list with 0 uses remaining is therefore "safe to produce," in the sense that no remaining reaction will require more of it so we know exactly how much of it we'll need.

The main calculation loop then looks at the list of reagents we want to produce and picks the first one it finds that has 0 uses remaining. It then finds the reaction that creates that chemical (remember that there's only one) and figures out how many times we need to do that reaction to produce the amount of that chemical we need. It then adds the amounts of the reagents we need for those reactions to the working list of required reagents and decrements each reagent in the list of the number of uses remaining for each reagent. The loop then restarts until the only reagent left in the list is ORE, and the amount there is the minimum amount we need to produce 1 FUEL.

This procedure works exactly the same if we want to find how much ORE is required to produce *N* FUEL; all we have to do is put *N* as the amount in the initial reagent list instead of 1. So for part 2, we can binary search on that number to find the answer pretty quickly.
