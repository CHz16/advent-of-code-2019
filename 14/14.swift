#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "14.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

struct Reaction {
    let reagents: [String: Int]
    let productChemical: String
    let productAmount: Int
}

var reactions: [String: Reaction] = [:]
var reagentUsesRequired = ["FUEL": 0]
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: .whitespaces)

    var reagents: [String: Int] = [:]
    for i in stride(from: 0, to: components.count - 3, by: 2) {
        let amount = Int(components[i])!
        let chemical = components[i + 1]
        reagents[chemical] = amount
        reagentUsesRequired[chemical, default: 0] += 1
    }

    let productAmount = Int(components[components.count - 2])!
    let productChemical = components[components.count - 1]
    reactions[productChemical] = Reaction(reagents: reagents, productChemical: productChemical, productAmount: productAmount)
}


func amountOfOre(forFuel fuelAmount: Int) -> Int {
    var reagentUsesRemaining = reagentUsesRequired
    var reagentsRequired = ["FUEL": fuelAmount]
    while reagentsRequired.count != 1 || reagentsRequired["ORE"] == nil {
        var candidateProduct: String?
        for chemical in reagentsRequired.keys {
            if reagentUsesRemaining[chemical]! == 0 {
                candidateProduct = chemical
                break
            }
        }

        guard let product = candidateProduct else {
            print("Couldn't find a clear reagent")
            print(reagentUsesRemaining)
            print(reagentsRequired)
            return -1 // lmao don't worry about it
        }

        let reaction = reactions[product]!
        let desiredAmount = reagentsRequired[product]!
        let numberOfReactions = desiredAmount / reaction.productAmount + (desiredAmount % reaction.productAmount != 0 ? 1 : 0)

        for (reagent, amount) in reaction.reagents {
            reagentsRequired[reagent, default: 0] += amount * numberOfReactions
            reagentUsesRemaining[reagent]! -= 1
        }

        reagentsRequired[product] = nil
    }
    return reagentsRequired["ORE"]!
}

print(amountOfOre(forFuel: 1))


// Part 2

print("--------------")

var rangeMin = 1, rangeMax = Int.max
while rangeMax > (rangeMin + 1) {
    let fuelAmount: Int
    if rangeMax == Int.max {
        // Still probing ahead to find the first hit
        fuelAmount = rangeMin
    } else {
        fuelAmount = (rangeMin + rangeMax) / 2
    }

    let oreAmount = amountOfOre(forFuel: fuelAmount)
    let overshot = oreAmount > 1000000000000
    if rangeMax == Int.max {
        // Still probing for the first hit
        if !overshot {
            // Probe forward by x2
            rangeMin *= 2
        } else {
            // Probing complete, set up the range for binary search
            rangeMax = rangeMin
            rangeMin /= 2
        }
    } else {
        // Adjust the bounds
        if !overshot {
            rangeMin = fuelAmount
        } else {
            rangeMax = fuelAmount
        }
    }
}

print(rangeMin)
