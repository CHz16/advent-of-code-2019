//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "6", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var tree: [String: [String]] = [:]
var parents: [String: String] = [:]

input.enumerateLines { (line, stop) in
    let bodies = line.components(separatedBy: ")")
    let orbiter = bodies[1], body = bodies[0]

    if tree[orbiter] == nil {
        tree[orbiter] = []
    }
    if tree[body] == nil {
        tree[body] = []
    }

    tree[body]!.append(orbiter)
    parents[orbiter] = body
}

var bodiesOrbitingCache: [String: Int] = [:]
func bodiesOrbiting(_ body: String) -> Int {
    if let c = bodiesOrbitingCache[body] {
        return c
    }

    let orbiters = tree[body]!
    var count = orbiters.count
    for orbiter in orbiters {
        count += bodiesOrbiting(orbiter)
    }
    bodiesOrbitingCache[body] = count
    return count
}

_ = bodiesOrbiting("COM")
print(bodiesOrbitingCache.reduce(0) { $0 + $1.1 } )


// Part 2

print("--------------")

func findPathFromCOM(to body: String) -> [String] {
    var path = [body]
    var currentObject = body
    while (currentObject != "COM") {
        currentObject = parents[currentObject]!
        path.append(currentObject)
    }
    return path.reversed()
}

let youPath = findPathFromCOM(to: parents["YOU"]!), santaPath = findPathFromCOM(to: parents["SAN"]!)
var commonAncestors = 0
while youPath[commonAncestors] == santaPath[commonAncestors] {
    commonAncestors += 1
}

print(youPath.count + santaPath.count - 2 * commonAncestors)
