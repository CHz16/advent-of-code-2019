Day 6: Universal Orbit Map
==========================

https://adventofcode.com/2019/day/6

Building a tree and then doing a couple of tree traversals, not too bad. Rather than make a bespoke data structure I just used a couple of dictionaries lmao

For part 1, I suspected there might be a lot of repeated orbit calculations that would slow down a playground, so I calculate the number of explicit and implicit orbits around COM, which should in the process calculate the orbits for every other body as well, and cache all of those results, which then just lets me sum them all up after.

For part 2, instead of doing a graph search, I know the path between YOU and SAN is going to be the path from one to the lowest common ancestor to the other, so I just find the lowest common ancestor and then add up the distances between that and YOU's parent and that and SAN's parent.

* Part 1: 775th place (15:53)
* Part 2: 717th place (28:05)
