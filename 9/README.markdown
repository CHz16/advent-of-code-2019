Day 9: Sensor Boost
===================

https://adventofcode.com/2019/day/9

Oh baby it's time for Even More VM Features

This one took me a while to get right because parameters that are write locations are treated differently than parameters that are values used in some other operation, and it took me too long to realize that. The old version of the VM completely disregarded the mode for write location parameters, because positional was the only one that mattered and we could handle that just by reading the value directly from the program code, but with the addition of relative mode, modes now matter! My first attempt at this was funneling the write location parameters through getParameter() like the other parameters, but they work differently than other parameters so that doesn't actually work, and it took be a bit to figure that out.
