#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "15.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


struct VM {
    enum State { case output(Int), waitingForInput, halted }

    var program: [Int]
    var inputs: [Int] = []

    var pc = 0
    var relativeBase = 0


    mutating func extendMemory(toLocation memoryLocation: Int) {
        if memoryLocation >= program.count {
            program += Array(repeating: 0, count: (memoryLocation - program.count + 1))
        }
    }

    mutating func write(value: Int, toMemoryLocation memoryLocation: Int) {
        extendMemory(toLocation: memoryLocation)
        program[memoryLocation] = value
    }

    mutating func getParameter(atIndex index: Int, withMode mode: Int) -> Int {
        if mode == 1 {
            // mode 1: immediate
            return program[index]
        }

        // mode 0: position
        // mode 2: relative
        let memoryLocation = (mode == 0) ? program[index] : (program[index] + relativeBase)
        extendMemory(toLocation: memoryLocation)
        return program[memoryLocation]
    }

    func getWriteLocation(atIndex index: Int, withMode mode: Int) -> Int {
        if mode == 0 {
            // mode 0: position
            return program[index]
        } else {
            // mode 2: relative
            return program[index] + relativeBase
        }
    }

    mutating func execute() -> State {
        outer: while pc < program.count {
            let opcode = program[pc] % 100
            let parameter1Mode, parameter2Mode, parameter3Mode: Int
            if program[pc] > 100 {
                parameter1Mode = (program[pc] / 100) % 10
            } else {
                parameter1Mode = 0
            }
            if program[pc] > 1000 {
                parameter2Mode = (program[pc] / 1000) % 10
            } else {
                parameter2Mode = 0
            }
            if program[pc] > 10000 {
                parameter3Mode = (program[pc] / 10000) % 10
            } else {
                parameter3Mode = 0
            }

            switch opcode {
            case 1:
                // +
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
                write(value: operand1 + operand2, toMemoryLocation: location)
                pc += 4
            case 2:
                // *
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
                write(value: operand1 * operand2, toMemoryLocation: location)
                pc += 4
            case 3:
                // input
                if inputs.isEmpty {
                    return .waitingForInput
                } else {
                    let input = inputs.removeFirst()
                    let location = getWriteLocation(atIndex: pc + 1, withMode: parameter1Mode)
                    write(value: input, toMemoryLocation: location)
                    pc += 2
                }
            case 4:
                // output
                let output = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                pc += 2
                return .output(output)
            case 5:
                // jump if true
                let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                if condition != 0 {
                    pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                } else {
                    pc += 3
                }
            case 6:
                // jump if false
                let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                if condition == 0 {
                    pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                } else {
                    pc += 3
                }
            case 7:
                // less than
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
                write(value: (operand1 < operand2) ? 1 : 0, toMemoryLocation: location)
                pc += 4
            case 8:
                // equals
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
                write(value: (operand1 == operand2) ? 1 : 0, toMemoryLocation: location)
                pc += 4
            case 9:
                // update relative base
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                relativeBase += operand1
                pc += 2
            case 99:
                // halt
                break outer
            default:
                // oh no
                print("unknown opcode", opcode)
                break outer
            }
        }

        return .halted
    }
}


// Part 1

enum Direction: Int {
    case north = 1, south = 2, west = 3, east = 4

    var opposite: Direction {
        switch self {
        case .north:
            return .south
        case .south:
            return .north
        case .west:
            return .east
        case .east:
            return .west
        }
    }
}

struct Destination {
    let x, y: Int
    let direction: Direction

    var destinationCoordinates: (Int, Int) {
        switch direction {
        case .north:
            return (x, y + 1)
        case .south:
            return (x, y - 1)
        case .west:
            return (x - 1, y)
        case .east:
            return (x + 1, y)
        }
    }
}

let initialProgram = input.components(separatedBy: ",").compactMap { Int($0) }
var vm = VM(program: initialProgram)

enum Tile: Int { case wall = 0, empty = 1, oxygen = 2, unknown = 3, start = 4 }
var grid = [0: [0: Tile.start]]
var backtrack: [Int: [Int: Direction]] = [:]

var locationStack = [
    Destination(x: 0, y: 0, direction: .north),
    Destination(x: 0, y: 0, direction: .south),
    Destination(x: 0, y: 0, direction: .west),
    Destination(x: 0, y: 0, direction: .east)
]
var minX = Int.max, maxX = Int.min, minY = Int.max, maxY = Int.min
var currentX = 0, currentY = 0
var oxygenX = 0, oxygenY = 0
while let destination = locationStack.popLast() {
    // If we already know what's there, then we can skip it
    let destinationCoordinates = destination.destinationCoordinates
    if grid[destinationCoordinates.1]?[destinationCoordinates.0] != nil {
        continue
    }

    // Backtrack to the destination start
    while currentX != destination.x || currentY != destination.y {
        let backtrackDirection = backtrack[currentY]![currentX]!
        vm.inputs = [backtrackDirection.rawValue]
        _ = vm.execute()
        let backtrackDestination = Destination(x: currentX, y: currentY, direction: backtrackDirection)
        let backtrackCoordinates = backtrackDestination.destinationCoordinates
        currentX = backtrackCoordinates.0
        currentY = backtrackCoordinates.1
    }

    // Try moving
    vm.inputs = [destination.direction.rawValue]
    guard case let .output(status) = vm.execute() else { break }
    if status == 0 {
        // Hit wall
        grid[destinationCoordinates.1, default: [:]][destinationCoordinates.0] = .wall
    } else {
        // We moved, so update current state and our knowledge of the maze
        currentX = destinationCoordinates.0
        currentY = destinationCoordinates.1
        grid[currentY, default: [:]][currentX] = Tile(rawValue: status)
        backtrack[currentY, default: [:]][currentX] = destination.direction.opposite

        // Add every adjacent square to the destination queue
        let newDestinations = [
            Destination(x: currentX, y: currentY, direction: .north),
            Destination(x: currentX, y: currentY, direction: .south),
            Destination(x: currentX, y: currentY, direction: .west),
            Destination(x: currentX, y: currentY, direction: .east)
        ]
        locationStack += newDestinations

        // Keep track of the bounds of the known area for printing purposes
        // Doing this here only means we won't print the boundary walls, which seems fine
        minX = min(minX, currentX)
        maxX = max(maxX, currentX)
        minY = min(minY, currentY)
        maxY = max(maxY, currentY)

        // Save the location of the oxygen system for later
        if status == 2 {
            oxygenX = currentX
            oxygenY = currentY
        }
    }
}

// Print the maze
for y in (minY...maxY).reversed() {
    var row = ""
    for x in minX...maxX {
        switch grid[y, default: [:]][x, default: .unknown] {
        case .wall:
            row += "#"
        case .empty:
            row += "."
        case .oxygen:
            row += "O"
        case .unknown:
            row += "?"
        case .start:
            row += "D"
        }
    }
    print(row)
}

// Backtrack from the oxygen to the start to find the distance
var moveCount = 0
currentX = oxygenX
currentY = oxygenY
while currentX != 0 || currentY != 0 {
    let backtrackDirection = backtrack[currentY]![currentX]!
    let backtrackDestination = Destination(x: currentX, y: currentY, direction: backtrackDirection)
    let backtrackCoordinates = backtrackDestination.destinationCoordinates
    currentX = backtrackCoordinates.0
    currentY = backtrackCoordinates.1

    moveCount += 1
}
print(moveCount)


// Part 2

print("--------------")

var time = -1
var oxygenLocations = [(oxygenX, oxygenY)]
grid[oxygenY]![oxygenX] = .empty // lmao we have to unset this from .oxygen first
while !oxygenLocations.isEmpty {
    var newOxygenLocations: [(Int, Int)] = []

    for (x, y) in oxygenLocations {
        let tile = grid[y, default: [:]][x, default: .wall]
        if tile == .wall || tile == .oxygen {
            continue
        }
        grid[y]![x] = .oxygen
        newOxygenLocations += [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)]
    }

    oxygenLocations = newOxygenLocations
    time += 1
}

print(time - 1)

