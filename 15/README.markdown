Day 15: Oxygen System
=====================

https://adventofcode.com/2019/day/15

First part implements a simple DFS backtracking maze navigation algorithm; it keeps a stack of locations to visit and every turn picks the top one, backtracks if necessary and goes there, and then adds all adjacent locations to the stack. We keep track of the backtracking data at the same time, so when we have to calculate the distance from the start to the oxygen system we can just read the path directly from there. This approach fails if there are loops in the maze, but there aren't so that's fine.

Second part does a BFS from the oxygen system to all other points in the maze, using the grid that we built up in the first part.

* Part 1: 237th place (42:16)
* Part 2: 211th place (56:00)
