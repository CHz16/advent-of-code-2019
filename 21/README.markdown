Day 21: Springdroid Adventure
=============================

https://adventofcode.com/2019/day/21

This puzzle is basically writing a Mario AI, which actually owns lmao

The boolean expression for my part 1 is `(!A || !B || !C) && D`, which says that if there's a pit somewhere in the first three squares in front of us and the fourth square is safe to jump to, then jump.

The part 2 expression extends the second condition: `(!A || !B || !C) && (D && (E || H))`. This says to jump only if the fourth square is safe to jump to AND it's safe from there to either walk forward (`E`) or immediately jump again (`H`).

I feel like it's probably possible to come up with some adversarial input where either of these expressions fail because you need information past the lookahead distance to make the correct decision, but they both work for my input so that's nice.
