#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "10.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


typealias Point = (x: Int, y: Int)

func manhattanDistanceBetween(_ a: Point, _ b: Point) -> Int {
    return abs(a.x - b.x) + abs(a.y - b.y)
}

func gcd(_ a: Int, _ b: Int) -> Int {
    if b == 0 {
        return a
    } else {
        return gcd(b, a % b)
    }
}

struct Trajectory: Hashable, Equatable, Comparable {
    let xDelta, yDelta: Int

    var angle: Double {
        let a = atan2(Double(xDelta), -Double(yDelta))
        if a >= 0 {
            return a
        } else {
            return 2 * Double.pi + a
        }
    }

    init(from: Point, to: Point) {
        let xd = to.x - from.x, yd = to.y - from.y
        if xd == 0 || yd == 0 {
            xDelta = (xd == 0) ? 0 : xd.signum()
            yDelta = (yd == 0) ? 0 : yd.signum()
        } else {
            let slopeGCD = gcd(abs(xd), abs(yd))
            xDelta = xd / slopeGCD
            yDelta = yd / slopeGCD
        }
    }

    static func < (lhs: Trajectory, rhs: Trajectory) -> Bool {
        return lhs.angle < rhs.angle
    }
}


// Part 1

var region: [[Bool]] = []
var asteroids: [Point] = []
for (row, line) in input.components(separatedBy: .newlines).enumerated() {
    var regionRow: [Bool] = []
    for (col, char) in line.enumerated() {
        let isAsteroid = (char == "#")
        regionRow.append(isAsteroid)
        if isAsteroid {
            asteroids.append(Point(x: col, y: row))
        }
    }
    region.append(regionRow)
}
region.removeLast() // trailing newline

var maxVisibleAsteroids = Int.min, laserLoc = Point(x: 0, y: 0)
for loc in asteroids {
    let visibleAsteroids = Set(asteroids.map { Trajectory(from: loc, to: $0) }).count - 1 // -1 to remove current asteroid
    if visibleAsteroids > maxVisibleAsteroids {
        print(loc, visibleAsteroids)
        maxVisibleAsteroids = visibleAsteroids
        laserLoc = loc
    }
}


// Part 2

print("--------------")

var remainingAsteroids = asteroids.filter { $0.x != laserLoc.x || $0.y != laserLoc.y }.map { (loc: $0, distance: manhattanDistanceBetween(laserLoc, $0), trajectory: Trajectory(from: laserLoc, to: $0)) }
remainingAsteroids.sort {
    if $0.trajectory != $1.trajectory {
        return $0.trajectory < $1.trajectory
    } else {
        return $0.distance < $1.distance
    }
}

var vaporizedAsteroids = 0, lastTrajectory = Trajectory(from: Point(x: 0, y: 0), to: Point(x: -1, y: -1000))
var i = 0
while vaporizedAsteroids < 200 {
    if remainingAsteroids[i].trajectory == lastTrajectory {
        i += 1
    } else {
        let vaporizedAsteroid = remainingAsteroids.remove(at: i)
        lastTrajectory = vaporizedAsteroid.trajectory
        vaporizedAsteroids += 1
        print(vaporizedAsteroids, vaporizedAsteroid)
    }

    if i >= remainingAsteroids.count {
        i = 0
    }
}
