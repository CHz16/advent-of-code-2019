#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "25.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


struct VM {
    enum State: Equatable { case output(Int), waitingForInput, halted, running }

    var program: [Int]
    var inputs: [Int] = []

    var pc = 0
    var relativeBase = 0


    mutating func extendMemory(toLocation memoryLocation: Int) {
        if memoryLocation >= program.count {
            program += Array(repeating: 0, count: (memoryLocation - program.count + 1))
        }
    }

    mutating func write(value: Int, toMemoryLocation memoryLocation: Int) {
        extendMemory(toLocation: memoryLocation)
        program[memoryLocation] = value
    }

    mutating func getParameter(atIndex index: Int, withMode mode: Int) -> Int {
        if mode == 1 {
            // mode 1: immediate
            return program[index]
        }

        // mode 0: position
        // mode 2: relative
        let memoryLocation = (mode == 0) ? program[index] : (program[index] + relativeBase)
        extendMemory(toLocation: memoryLocation)
        return program[memoryLocation]
    }

    func getWriteLocation(atIndex index: Int, withMode mode: Int) -> Int {
        if mode == 0 {
            // mode 0: position
            return program[index]
        } else {
            // mode 2: relative
            return program[index] + relativeBase
        }
    }

    mutating func execute() -> State {
        while true {
            let result = step()
            if result == .running {
                continue
            }
            return result
        }
    }

    mutating func step() -> State {
        if pc < 0 || pc >= program.count {
            return .halted
        }

        let opcode = program[pc] % 100
        let parameter1Mode, parameter2Mode, parameter3Mode: Int
        if program[pc] > 100 {
            parameter1Mode = (program[pc] / 100) % 10
        } else {
            parameter1Mode = 0
        }
        if program[pc] > 1000 {
            parameter2Mode = (program[pc] / 1000) % 10
        } else {
            parameter2Mode = 0
        }
        if program[pc] > 10000 {
            parameter3Mode = (program[pc] / 10000) % 10
        } else {
            parameter3Mode = 0
        }

        switch opcode {
        case 1:
            // +
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
            write(value: operand1 + operand2, toMemoryLocation: location)
            pc += 4
        case 2:
            // *
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
            write(value: operand1 * operand2, toMemoryLocation: location)
            pc += 4
        case 3:
            // input
            if inputs.isEmpty {
                return .waitingForInput
            } else {
                let input = inputs.removeFirst()
                let location = getWriteLocation(atIndex: pc + 1, withMode: parameter1Mode)
                write(value: input, toMemoryLocation: location)
                pc += 2
            }
        case 4:
            // output
            let output = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            pc += 2
            return .output(output)
        case 5:
            // jump if true
            let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            if condition != 0 {
                pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            } else {
                pc += 3
            }
        case 6:
            // jump if false
            let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            if condition == 0 {
                pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            } else {
                pc += 3
            }
        case 7:
            // less than
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
            write(value: (operand1 < operand2) ? 1 : 0, toMemoryLocation: location)
            pc += 4
        case 8:
            // equals
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
            write(value: (operand1 == operand2) ? 1 : 0, toMemoryLocation: location)
            pc += 4
        case 9:
            // update relative base
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            relativeBase += operand1
            pc += 2
        case 99:
            // halt
            return .halted
        default:
            // oh no
            print("unknown opcode", opcode)
            return .halted
        }

        return .running
    }

}


// Part 1

func makeInputForVM(_ s: String) -> [Int] {
    return s.map { Int($0.asciiValue!) } + [10]
}

let initialProgram = input.components(separatedBy: ",").compactMap { Int($0) }

var vm = VM(program: initialProgram)
var output = ""
var inventory: [String] = []
var lastSafeVM = vm
game: while true {
    let result = vm.execute()
    if result == .halted {
        print(output)
        print("\n---------\nrewinding\n---------\n")
        output = ""
        vm = lastSafeVM
        continue
    } else if case let .output(value) = result {
        output.append(Character(UnicodeScalar(value)!))
    } else if result == .waitingForInput {
        lastSafeVM = vm
        print(output)
        output = ""

        let command = readLine()!
        let commandComponents = command.components(separatedBy: " ")
        if commandComponents[0] == "take" {
            inventory.append(commandComponents[1...].joined(separator: " "))
        }

        if command != "cheat" {
            vm.inputs = command.map { Int($0.asciiValue!) } + [10]
            continue
        }

        var trialCombination = Array(repeating: true, count: inventory.count)
        repeat {
            // try to pass checkpoint
            print(trialCombination)
            vm.inputs = makeInputForVM("west")
            var trialResult = ""
            while case let .output(value) = vm.execute() {
                trialResult.append(Character(UnicodeScalar(value)!))
            }
            if !trialResult.contains("== Security Checkpoint ==") {
                print(trialResult)
                break game
            }

            var nextCombination = trialCombination
            var currentDigit = nextCombination.count - 1
            while currentDigit >= 0 {
                if nextCombination[currentDigit] {
                    nextCombination[currentDigit] = false
                    break
                } else {
                    nextCombination[currentDigit] = true
                    currentDigit -= 1
                }
            }

            for i in 0..<trialCombination.count {
                if trialCombination[i] && !nextCombination[i] {
                    vm.inputs = ("drop " + inventory[i]).map { Int($0.asciiValue!) } + [10]
                    while case .output(_) = vm.execute() { }
                } else if !trialCombination[i] && nextCombination[i] {
                    vm.inputs = ("take " + inventory[i]).map { Int($0.asciiValue!) } + [10]
                    while case .output(_) = vm.execute() { }
                }
            }

            trialCombination = nextCombination
        } while !trialCombination.allSatisfy { $0 }
    }
}
