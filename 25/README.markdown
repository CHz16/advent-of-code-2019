Day 25: Cryostasis
==================

https://adventofcode.com/2019/day/25

lawl ending this year with an interactive fiction game, sure

Because it was faster, for my first solution I actually just played through the game myself. I actually had to read text input for the first time, oh no! I had my input handler keep track of whenever I picked up an item, and I checked for a special command "cheat" which would try each combination of items in turn to see if it was the correct one.

But I actually wanted to write a program to play the whole game on its own without me telling it where to go or what to pick up, so that's in `25-2.swift`. It parses all the room descriptions and does a DFS through the maze, picking up all items it can along the way, and then heads over to the security checkpoint and does the brute-force combo search. It also properly detects the items that kill the game by keeping a backup copy of the VM and restoring from that if the game halts. I did have to hardcode in the infinite loop and giant electromagnet items because those break the game in a nastier way that's not immediately detectable.

* Part 1: 300th place (57:34)
