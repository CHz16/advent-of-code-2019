#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "25.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


struct VM {
    enum State: Equatable { case output(Int), waitingForInput, halted, running }

    var program: [Int]
    var inputs: [Int] = []

    var pc = 0
    var relativeBase = 0


    mutating func extendMemory(toLocation memoryLocation: Int) {
        if memoryLocation >= program.count {
            program += Array(repeating: 0, count: (memoryLocation - program.count + 1))
        }
    }

    mutating func write(value: Int, toMemoryLocation memoryLocation: Int) {
        extendMemory(toLocation: memoryLocation)
        program[memoryLocation] = value
    }

    mutating func getParameter(atIndex index: Int, withMode mode: Int) -> Int {
        if mode == 1 {
            // mode 1: immediate
            return program[index]
        }

        // mode 0: position
        // mode 2: relative
        let memoryLocation = (mode == 0) ? program[index] : (program[index] + relativeBase)
        extendMemory(toLocation: memoryLocation)
        return program[memoryLocation]
    }

    func getWriteLocation(atIndex index: Int, withMode mode: Int) -> Int {
        if mode == 0 {
            // mode 0: position
            return program[index]
        } else {
            // mode 2: relative
            return program[index] + relativeBase
        }
    }

    mutating func execute() -> State {
        while true {
            let result = step()
            if result == .running {
                continue
            }
            return result
        }
    }

    mutating func step() -> State {
        if pc < 0 || pc >= program.count {
            return .halted
        }

        let opcode = program[pc] % 100
        let parameter1Mode, parameter2Mode, parameter3Mode: Int
        if program[pc] > 100 {
            parameter1Mode = (program[pc] / 100) % 10
        } else {
            parameter1Mode = 0
        }
        if program[pc] > 1000 {
            parameter2Mode = (program[pc] / 1000) % 10
        } else {
            parameter2Mode = 0
        }
        if program[pc] > 10000 {
            parameter3Mode = (program[pc] / 10000) % 10
        } else {
            parameter3Mode = 0
        }

        switch opcode {
        case 1:
            // +
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
            write(value: operand1 + operand2, toMemoryLocation: location)
            pc += 4
        case 2:
            // *
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
            write(value: operand1 * operand2, toMemoryLocation: location)
            pc += 4
        case 3:
            // input
            if inputs.isEmpty {
                return .waitingForInput
            } else {
                let input = inputs.removeFirst()
                let location = getWriteLocation(atIndex: pc + 1, withMode: parameter1Mode)
                write(value: input, toMemoryLocation: location)
                pc += 2
            }
        case 4:
            // output
            let output = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            pc += 2
            return .output(output)
        case 5:
            // jump if true
            let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            if condition != 0 {
                pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            } else {
                pc += 3
            }
        case 6:
            // jump if false
            let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            if condition == 0 {
                pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            } else {
                pc += 3
            }
        case 7:
            // less than
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
            write(value: (operand1 < operand2) ? 1 : 0, toMemoryLocation: location)
            pc += 4
        case 8:
            // equals
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = getWriteLocation(atIndex: pc + 3, withMode: parameter3Mode)
            write(value: (operand1 == operand2) ? 1 : 0, toMemoryLocation: location)
            pc += 4
        case 9:
            // update relative base
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            relativeBase += operand1
            pc += 2
        case 99:
            // halt
            return .halted
        default:
            // oh no
            print("unknown opcode", opcode)
            return .halted
        }

        return .running
    }

}


// Part 1

let initialProgram = input.components(separatedBy: ",").compactMap { Int($0) }
var vm = VM(program: initialProgram)

func readOutput() -> (String, VM.State) {
    var output = ""
    while case let .output(value) = vm.execute() {
        output.append(Character(UnicodeScalar(value)!))
    }
    return (output, vm.execute())
}

func makeInputForVM(_ s: String) -> [Int] {
    return s.map { Int($0.asciiValue!) } + [10]
}

enum Direction: String {
    case north = "north", south = "south", east = "east", west = "west"

    var opposite: Direction {
        switch self {
        case .north:
            return .south
        case .south:
            return .north
        case .west:
            return .east
        case .east:
            return .west
        }
    }
}


var roomLayout: [String: [Direction: String]] = [:]
var backtrack: [String: Direction] = [:]
var inventory: [String] = []

var currentRoom = "Hull Breach"
var stack = [(room: currentRoom, direction: Direction.west)]
var initialScan = true
var visited: Set<String> = Set()
while let (room, direction) = stack.popLast() {
    // Backtrack if necessary
    while currentRoom != room {
        let backtrackDirection = backtrack[currentRoom]!
        vm.inputs = makeInputForVM(backtrackDirection.rawValue)
        currentRoom = roomLayout[currentRoom]![backtrackDirection]!
        _ = readOutput()
    }

    // Move to the next room
    if !initialScan {
        vm.inputs = makeInputForVM(direction.rawValue)
    }
    let (output, _) = readOutput()

    // Scan the room
    var exits: [Direction] = []
    var items: [String] = []
    var newRoomName = ""
    output.enumerateLines { (line, stop) in
        let components = line.components(separatedBy: .whitespaces)
        if components.count > 1 && components[0] == "==" {
            newRoomName = components[1..<(components.count - 1)].joined(separator: " ")
        } else if components.count > 1 && components[0] == "-" {
            if components[1] == "north" {
                exits.append(.north)
            } else if components[1] == "south" {
                exits.append(.south)
            } else if components[1] == "east" {
                exits.append(.east)
            } else if components[1] == "west" {
                exits.append(.west)
            } else {
                items.append(components[1...].joined(separator: " "))
            }
        }
    }

    // Handle loops and the secure room that boots you
    if visited.contains(newRoomName) {
        continue
    }
    visited.insert(newRoomName)

    // Add the path we just took to the layout
    roomLayout[currentRoom, default: [:]][direction] = newRoomName
    roomLayout[newRoomName, default: [:]][direction.opposite] = currentRoom

    // Add the exits of the new room to the DFS stack
    backtrack[newRoomName] = direction.opposite
    for exitDirection in exits {
        if initialScan || exitDirection != direction.opposite {
            stack.append((room: newRoomName, direction: exitDirection))
        }
    }

    // Try to pick up all the items
    items = items.filter { $0 != "infinite loop" && $0 != "giant electromagnet" } // these don't halt the game if you try to pick them up but they're bad
    for item in items {
        let backupVM = vm
        vm.inputs = makeInputForVM("take " + item)
        let (_, state) = readOutput()
        if state == .halted {
            print("skipped \(item) in \(newRoomName)")
            vm = backupVM
        } else {
            print("picked up \(item) in \(newRoomName)")
            inventory.append(item)
        }
    }

    currentRoom = newRoomName
    initialScan = false
}

// Backtrack to the starting room (Hull Breach) using the backtrack tree, and then go to the Security Checkpoint by also using the backtrack tree and then reversing that path
print("\nitem scan complete, heading to Security Checkpoint")
var pathToSecurityCheckpoint: [Direction] = []
while currentRoom != "Hull Breach" {
    let backtrackDirection = backtrack[currentRoom]!
    pathToSecurityCheckpoint.append(backtrackDirection)
    currentRoom = roomLayout[currentRoom]![backtrackDirection]!
}
var secondHalfPath: [Direction] = []
currentRoom = "Security Checkpoint"
while currentRoom != "Hull Breach" {
    let backtrackDirection = backtrack[currentRoom]!
    secondHalfPath.append(backtrackDirection.opposite)
    currentRoom = roomLayout[currentRoom]![backtrackDirection]!
}
pathToSecurityCheckpoint += secondHalfPath.reversed()
for direction in pathToSecurityCheckpoint {
    vm.inputs = makeInputForVM(direction.rawValue)
    _ = readOutput()
}

// Try every item combination until we hit the right one
// We'll represent an inventory combination as a binary number: bit 1 being true means we're holding item 1 and false means we're not. This means we can try every combo just by staring at 11...1 and subtracting 1 until we hit 00...0
var trialCombination = Array(repeating: true, count: inventory.count)
repeat {
    // See if the combination we're currently holding is correct
    vm.inputs = makeInputForVM("west")
    let (output, state) = readOutput()
    if state == .halted {
        print("correct items:", zip(trialCombination, inventory).filter { $0.0 }.map { $0.1 })
        print(output)
        break
    }

    // Subtract 1 from the inventory number and update the in-game inventory
    var currentDigit = trialCombination.count - 1
    while currentDigit >= 0 {
        if trialCombination[currentDigit] {
            vm.inputs = ("drop " + inventory[currentDigit]).map { Int($0.asciiValue!) } + [10]
            _ = readOutput()
            trialCombination[currentDigit] = false
            break
        } else {
            vm.inputs = ("take " + inventory[currentDigit]).map { Int($0.asciiValue!) } + [10]
            _ = readOutput()
            trialCombination[currentDigit] = true
            currentDigit -= 1
        }
    }
} while !trialCombination.allSatisfy { $0 }
