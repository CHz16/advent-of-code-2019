#!/usr/bin/env xcrun swift

let minPassword = 172930, maxPassword = 683082

// Part 1

func isValidPassword(_ password: Int) -> Bool {
    var digits: [Int] = []
    var base = 1;
    for _ in 0..<6 {
        digits.insert((password / base) % 10, at: 0)
        base *= 10
    }

    var hasDouble = false
    for i in 0..<5 {
        if digits[i] > digits[i + 1] {
            return false
        }
        hasDouble = hasDouble || (digits[i] == digits[i + 1])
    }
    return hasDouble
}

print((minPassword...maxPassword).filter(isValidPassword).count)


// Part 2

print("--------------")

func isValidPassword2(_ password: Int) -> Bool {
    var digits: [Int] = []
    var base = 1;
    for _ in 0..<6 {
        digits.insert((password / base) % 10, at: 0)
        base *= 10
    }

    var runLengths: [Int] = []
    var currentDigit = digits[0], currentRunLength = 1
    for i in 1..<6 {
        if digits[i - 1] > digits[i] {
            return false
        }

        if currentDigit == digits[i] {
            currentRunLength += 1
        } else {
            runLengths.append(currentRunLength)
            currentDigit = digits[i]
            currentRunLength = 1
        }
    }
    runLengths.append(currentRunLength)
    return runLengths.contains(2)
}

print((minPassword...maxPassword).filter(isValidPassword2).count)
