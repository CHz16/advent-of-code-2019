Day 4: Secure Container
=======================

https://adventofcode.com/2019/day/4

This could absolutely be done better than decomposing each number into a new array each loop, but watch me not care!

Part 2's extra check was done by calculating the run lengths of every consecutive sequence of digits in the password and then checking at the end of any of those lengths is exactly 2.

* Part 1: 962nd place (8:00)
* Part 2: 628nd place (14:07)
