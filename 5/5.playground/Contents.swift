//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "5", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


func execute(program: [Int], readingFromInput readInput: () -> Int) {
    var program = program
    func getParameter(atIndex index: Int, withMode mode: Int) -> Int {
        if mode == 0 {
            // mode 0: position
            return program[program[index]]
        } else {
            // mode 1: immediate
            return program[index]
        }
    }
    
    var pc = 0
    outer: while pc < program.count {
        let opcode = program[pc] % 100
        let parameter1Mode, parameter2Mode, parameter3Mode: Int
        if program[pc] > 100 {
            parameter1Mode = (program[pc] / 100) % 10
        } else {
            parameter1Mode = 0
        }
        if program[pc] > 1000 {
            parameter2Mode = (program[pc] / 1000) % 10
        } else {
            parameter2Mode = 0
        }
        if program[pc] > 10000 {
            parameter3Mode = (program[pc] / 10000) % 10
        } else {
            parameter3Mode = 0
        }

        switch opcode {
        case 1:
            // +
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = program[pc + 3]
            program[location] = operand1 + operand2
            pc += 4
        case 2:
            // *
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = program[pc + 3]
            program[location] = operand1 * operand2
            pc += 4
        case 3:
            // input
            let input = readInput()
            print("input: \(input)")
            program[program[pc + 1]] = input
            pc += 2
        case 4:
            // output
            let output = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            print(output)
            pc += 2
        case 5:
            // jump if true
            let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            if condition != 0 {
                pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            } else {
                pc += 3
            }
        case 6:
            // jump if false
            let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            if condition == 0 {
                pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            } else {
                pc += 3
            }
        case 7:
            // less than
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = program[pc + 3]
            program[location] = (operand1 < operand2) ? 1 : 0
            pc += 4
        case 8:
            // equals
            let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
            let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
            let location = program[pc + 3]
            program[location] = (operand1 == operand2) ? 1 : 0
            pc += 4
        default:
            break outer
        }
    }
}


// Part 1

let initialProgram = input.components(separatedBy: ",").compactMap { Int($0) }

execute(program: initialProgram, readingFromInput: { 1 })


// Part 2

print("--------------")

execute(program: initialProgram, readingFromInput: { 5 })
