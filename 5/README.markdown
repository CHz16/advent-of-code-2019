Day 5: Sunny with a Chance of Asteroids
=======================================

https://adventofcode.com/2019/day/5

Yep here we go, had to update the VM to a "proper" "architecture" now. Ended up basically just rewriting this one from scratch. I probably could've solved this much faster, but I ended up messing up accessing positional values and immediate values repeatedly.

I don't think Swift playgrounds have a way to get user input using `readLine()` or something like that, hence the hilarious solution of a  function passed into the VM lmao

* Part 1: 478th place (27:27)
* Part 2: 375th place (35:45)
