Day 22: Slam Shuffle
====================

https://adventofcode.com/2019/day/2

So this is, uh, a Project Euler math problem, not a programming problem. It took me four and a half hours to solve part 2 and I moved up 300 spots on the leaderboard over my part 1

Part 1 you can totally solve just by simulation, which I do. My code only keeps track of the single position that we care about, which is nice.

Part 2 is a thing.

My first observation was that all the shuffling operations are easily invertible, so I can play the shuffle in reverse to find what card will end up in a given position. Since I'm clearly not intended to execute all one hundred trillion shuffles, assuredly what will happen here is that eventually the positions will cycle, at which point it'll be very easy to calculate the final result.

So I wrote that up and ran it, and after like a half an hour or something it had calculated over fourteen million positions and still hadn't found a cycle, so clearly that's the wrong approach. Great.

Which means we actually do have to directly calculate the result of one hundred trillion shuffles, somehow.

So after a bunch of grasping at straws and staring at my screen, I decided to see if I could re-cast each of the *inverted* shuffling operations as transposition functions that I could compose, the idea being that maybe I could combine the entire shuffle definition into just one function that then behaves "well" if I iterate it one hundred trillion times

So I worked it out and it turns out that each of the three shuffling operations can be expressed as `ax + b (mod m)`, where `x` is the position of the card before operation and `a` and `b` are some constants:

* Deal into new stack: `(size - 1) - x = size - 1 - ax - b = (-a)x + (size - 1 - b)`
* Cut N: `x + N = ax + (b + N)`
* Deal with increment N: `x / N = (a/N)x + (b/N)`

And the identity operation is just `1x + 0`.

The nice thing about these `ax + b` expressions is that two of them compose into an `ax + b` expression, so if I compose the entire inverted shuffle, I can produce two numbers `a` and `b` with which I can immediately calculate the card that will end up in a given position after one shuffle.

However, I need to rewind many shuffles, so what does this look like when I iterate it?

* `f(x) = ax+b`
* `f^2(x) = a^2*x + (ab + b)`
* `f^3(x) = a^3*x + (a^2*b + ab + b)`
* `f^4(x) = a^4*x + (a^3*b + a^2*b + ab + b)`
* `f^N(x) = a^N*x + (geometric series from 1 to a^N-1)b`

`a^N (mod m)` is pretty quick to calculate for enormous `N`s with exponentiation by squaring. The formula for the geometric series is `(1 - a^N) / (1 - a) (mod m)`, which also pretty straightforward, we can reuse the value of `a^N` and recast the division as multiplication by the modular inverse (the problems intentionally give us numbers so that modular inversion works). There's some big trouble with overflowing integers so I had to write a multiplication & modulus function using Swift's Decimal class, but otherwise the implementation is pretty straightforward I think.

Like I said, this is a math problem, not a programming problem.
