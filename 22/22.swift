#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "22.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

enum Technique { case dealIntoNewStack, cut(Int), dealWithIncrement(Int) }
var shuffle: [Technique] = []
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: .whitespaces)
    if components[1] == "into" {
        shuffle.append(.dealIntoNewStack)
    } else if components[0] == "cut" {
        shuffle.append(.cut(Int(components[1])!))
    } else if components[1] == "with" {
        shuffle.append(.dealWithIncrement(Int(components[3])!))
    }
}

let deckSize = 10007

var trackedPosition = 2019
for technique in shuffle {
    switch technique {
    case .dealIntoNewStack:
        trackedPosition = deckSize - 1 - trackedPosition
    case let .cut(amount):
        var amount = amount
        if amount < 0 {
            amount += deckSize
        }
        if trackedPosition < amount {
            trackedPosition += deckSize - amount
        } else {
            trackedPosition -= amount
        }
    case let .dealWithIncrement(increment):
        trackedPosition = (trackedPosition * increment) % deckSize
    }
}
print(trackedPosition)


// Part 2

print("--------------")

func modularInverse(_ a: Int, modulo: Int) -> Int {
    // Extended Euclidian algo, haven't written this in a while so I just straight copied this from pseudocode on Wikipedia lmao
    var t = 0, newT = 1
    var r = modulo, newR = a
    while newR != 0 {
        let quotient = r / newR
        (t, newT) = (newT, t - quotient * newT)
        (r, newR) = (newR, r - quotient * newR)
    }
    return (t < 0) ? t + modulo : t
}

func exponentiateBySquaring(_ a: Int, _ n: Int, modulo: Int) -> Int {
    if n == 0 {
        return 1
    } else if n == 1 {
        return a
    }

    let aSquared = largeModularMultiplication(a, a, modulo: modulo)
    let continuedResult = exponentiateBySquaring(aSquared, n / 2, modulo: modulo)
    if n % 2 == 0 {
        return continuedResult
    } else {
        return largeModularMultiplication(a, continuedResult, modulo: modulo)
    }
}

func largeModularMultiplication(_ a: Int, _ b: Int, modulo: Int) -> Int {
    let product = Decimal(a) * Decimal(b)
    let divisor = Decimal(modulo)
    var divisionResult = product / divisor
    var quotient = Decimal()
    NSDecimalRound(&quotient, &divisionResult, 0, .down)
    return NSDecimalNumber(decimal: product - divisor * quotient).intValue
}


let part2DeckSize = 119315717514047
let numberOfShuffles = 101741582076661
let soughtPosition = 2020

var a = 1, b = 0 // identity transformation
for technique in shuffle.reversed() {
    switch technique {
    case .dealIntoNewStack:
        // (size - 1) - x = size - 1 - ax - b = (-a)x + (size - 1 - b)
        // f(a, b) = (-a mod size, size - 1 - b)
        a = part2DeckSize - a
        b = part2DeckSize - 1 - b
    case let .cut(amount):
        // x + N = ax + (b + N)
        // f(a, b) = (a, b + N mod size)
        b = (b + amount + part2DeckSize) % part2DeckSize
    case let .dealWithIncrement(increment):
        // x / N = (a/N)x + (b/N)
        // f(a, b) = (a/N mod size, b/N mod size)
        let incrementInverse = modularInverse(increment, modulo: part2DeckSize)
        a = largeModularMultiplication(a, incrementInverse, modulo: part2DeckSize)
        b = largeModularMultiplication(b, incrementInverse, modulo: part2DeckSize)
    }
}

let aN = exponentiateBySquaring(a, numberOfShuffles, modulo: part2DeckSize)
let geometricSeries: Int
if a == 1 {
    geometricSeries = numberOfShuffles
} else {
    geometricSeries = largeModularMultiplication(
        1 - aN + part2DeckSize,
        modularInverse(1 - a + part2DeckSize, modulo: part2DeckSize),
        modulo: part2DeckSize
    )
}

let position = (
    largeModularMultiplication(aN, soughtPosition, modulo: part2DeckSize) +
    largeModularMultiplication(b, geometricSeries, modulo: part2DeckSize)
) % part2DeckSize
print(position)
