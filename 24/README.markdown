Day 24: Planet of Discord
=========================

https://adventofcode.com/2019/day/24

Just a straightforward Life-style cellular automaton simulation. Part 2 is just the same thing but with a really weird adjacency graph. I'm sure part 2 puts out some very nicely cacheable structure but I sure just do all the calculations every time, it runs in less than a minute so that's fine

For unknown and probably bad reasons, I decided to manually write out all of the adjacency cases for part 2 instead of programmatically generating them. Maybe that hurt me in the standings!

* Part 1: 236th place (16:16)
* Part 2: 346th place (1:14:43)
