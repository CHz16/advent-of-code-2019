#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "24.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

func stepLayout(_ layout: [[Bool]]) -> [[Bool]] {
    var nextLayout = layout
    for row in 0..<layout.count {
        for col in 0..<layout[0].count {
            let aliveAdjacents = [
                (row, col - 1),
                (row - 1, col),
                (row + 1, col),
                (row, col + 1)
                ].filter { $0.0 >= 0 && $0.0 < layout.count && $0.1 >= 0 && $0.1 < layout[0].count && layout[$0.0][$0.1] }.count
            if layout[row][col] && aliveAdjacents != 1 {
                nextLayout[row][col] = false
            } else if !layout[row][col] && (aliveAdjacents == 1 || aliveAdjacents == 2) {
                nextLayout[row][col] = true
            }
        }
    }
    return nextLayout
}

func printLayout(_ layout: [[Bool]]) {
    for row in layout {
        print(row.map { $0 ? "#" : "." }.joined())
    }
}

var layout: [[Bool]] = []
input.enumerateLines { (line, stop) in
    layout.append(line.map { $0 == "#" })
}

var seenLayouts = Set([layout])
while true {
    let nextLayout = stepLayout(layout)
    if seenLayouts.contains(nextLayout) {
        var biodiversityRating = 0
        for row in 0..<nextLayout.count {
            for col in 0..<nextLayout[0].count {
                if nextLayout[row][col] {
                    biodiversityRating += 1 << (row * nextLayout.count + col)
                }
            }
        }
        printLayout(nextLayout)
        print(biodiversityRating)
        break
    }

    seenLayouts.insert(nextLayout)
    layout = nextLayout
}


// Part 2

print("--------------")

let recursiveAdjacents = [
    0: [
        0: [(layerDiff: -1, row: 1, col: 2), (layerDiff: 0, row: 0, col: 1), (layerDiff: 0, row: 1, col: 0), (layerDiff: -1, row: 2, col: 1)],
        1: [(layerDiff: -1, row: 1, col: 2), (layerDiff: 0, row: 0, col: 2), (layerDiff: 0, row: 1, col: 1), (layerDiff: 0, row: 0, col: 0)],
        2: [(layerDiff: -1, row: 1, col: 2), (layerDiff: 0, row: 0, col: 3), (layerDiff: 0, row: 1, col: 2), (layerDiff: 0, row: 0, col: 1)],
        3: [(layerDiff: -1, row: 1, col: 2), (layerDiff: 0, row: 0, col: 4), (layerDiff: 0, row: 1, col: 3), (layerDiff: 0, row: 0, col: 2)],
        4: [(layerDiff: -1, row: 1, col: 2), (layerDiff: -1, row: 2, col: 3), (layerDiff: 0, row: 1, col: 4), (layerDiff: 0, row: 0, col: 3)]
        ],
    1: [
        0: [(layerDiff: 0, row: 0, col: 0), (layerDiff: 0, row: 1, col: 1), (layerDiff: 0, row: 2, col: 0), (layerDiff: -1, row: 2, col: 1)],
        1: [(layerDiff: 0, row: 0, col: 1), (layerDiff: 0, row: 1, col: 2), (layerDiff: 0, row: 2, col: 1), (layerDiff: 0, row: 1, col: 0)],
        2: [(layerDiff: 0, row: 0, col: 2), (layerDiff: 0, row: 1, col: 3), (layerDiff: 1, row: 0, col: 0), (layerDiff: 1, row: 0, col: 1), (layerDiff: 1, row: 0, col: 2), (layerDiff: 1, row: 0, col: 3), (layerDiff: 1, row: 0, col: 4), (layerDiff: 0, row: 1, col: 1)],
        3: [(layerDiff: 0, row: 0, col: 3), (layerDiff: 0, row: 1, col: 4), (layerDiff: 0, row: 2, col: 3), (layerDiff: 0, row: 1, col: 2)],
        4: [(layerDiff: 0, row: 0, col: 4), (layerDiff: -1, row: 2, col: 3), (layerDiff: 0, row: 2, col: 4), (layerDiff: 0, row: 1, col: 3)]
        ],
    2: [
        0: [(layerDiff: 0, row: 1, col: 0), (layerDiff: 0, row: 2, col: 1), (layerDiff: 0, row: 3, col: 0), (layerDiff: -1, row: 2, col: 1)],
        1: [(layerDiff: 0, row: 1, col: 1), (layerDiff: 1, row: 0, col: 0), (layerDiff: 1, row: 1, col: 0), (layerDiff: 1, row: 2, col: 0), (layerDiff: 1, row: 3, col: 0), (layerDiff: 1, row: 4, col: 0), (layerDiff: 0, row: 3, col: 1), (layerDiff: 0, row: 2, col: 0)],
        2: [],
        3: [(layerDiff: 0, row: 1, col: 3), (layerDiff: 0, row: 2, col: 4), (layerDiff: 0, row: 3, col: 3), (layerDiff: 1, row: 0, col: 4), (layerDiff: 1, row: 1, col: 4), (layerDiff: 1, row: 2, col: 4), (layerDiff: 1, row: 3, col: 4), (layerDiff: 1, row: 4, col: 4)],
        4: [(layerDiff: 0, row: 1, col: 4), (layerDiff: -1, row: 2, col: 3), (layerDiff: 0, row: 3, col: 4), (layerDiff: 0, row: 2, col: 3)]
        ],
    3: [
        0: [(layerDiff: 0, row: 2, col: 0), (layerDiff: 0, row: 3, col: 1), (layerDiff: 0, row: 4, col: 0), (layerDiff: -1, row: 2, col: 1)],
        1: [(layerDiff: 0, row: 2, col: 1), (layerDiff: 0, row: 3, col: 2), (layerDiff: 0, row: 4, col: 1), (layerDiff: 0, row: 3, col: 0)],
        2: [(layerDiff: 1, row: 4, col: 0), (layerDiff: 1, row: 4, col: 1), (layerDiff: 1, row: 4, col: 2), (layerDiff: 1, row: 4, col: 3), (layerDiff: 1, row: 4, col: 4), (layerDiff: 0, row: 3, col: 3), (layerDiff: 0, row: 4, col: 2), (layerDiff: 0, row: 3, col: 1)],
        3: [(layerDiff: 0, row: 2, col: 3), (layerDiff: 0, row: 3, col: 4), (layerDiff: 0, row: 4, col: 3), (layerDiff: 0, row: 3, col: 2)],
        4: [(layerDiff: 0, row: 2, col: 4), (layerDiff: -1, row: 2, col: 3), (layerDiff: 0, row: 4, col: 4), (layerDiff: 0, row: 3, col: 3)],
        ],
    4: [
        0: [(layerDiff: 0, row: 3, col: 0), (layerDiff: 0, row: 4, col: 1), (layerDiff: -1, row: 3, col: 2), (layerDiff: -1, row: 2, col: 1)],
        1: [(layerDiff: 0, row: 3, col: 1), (layerDiff: 0, row: 4, col: 2), (layerDiff: -1, row: 3, col: 2), (layerDiff: 0, row: 4, col: 0)],
        2: [(layerDiff: 0, row: 3, col: 2), (layerDiff: 0, row: 4, col: 3), (layerDiff: -1, row: 3, col: 2), (layerDiff: 0, row: 4, col: 1)],
        3: [(layerDiff: 0, row: 3, col: 3), (layerDiff: 0, row: 4, col: 4), (layerDiff: -1, row: 3, col: 2), (layerDiff: 0, row: 4, col: 2)],
        4: [(layerDiff: 0, row: 3, col: 4), (layerDiff: -1, row: 2, col: 3), (layerDiff: -1, row: 3, col: 2), (layerDiff: 0, row: 4, col: 3)]
        ]
]

func isEmptySlice(_ slice: [[Bool]]) -> Bool {
    return slice.flatMap { $0 }.allSatisfy { !$0 }
}

func printRecursiveLayout(_ layout: [Int: [[Bool]]]) {
    let minLayer = Array(layout.keys).min()!
    let maxLayer = Array(layout.keys).max()!

    for layer in minLayer...maxLayer {
        print(layer)
        printLayout(layout[layer]!)
        print("")
    }
    print("~")
}


var recursiveLayout: [Int: [[Bool]]] = [0: []]
input.enumerateLines { (line, stop) in
    recursiveLayout[0]!.append(line.map { $0 == "#" })
}
recursiveLayout[0]![2][2] = false // just in case lmao

let blankLayer = Array(repeating: Array(repeating: false, count: 5), count: 5)
var minLayer = 0, maxLayer = 0
for _ in 0..<200 {
    minLayer -= 1
    recursiveLayout[minLayer] = blankLayer
    maxLayer += 1
    recursiveLayout[maxLayer] = blankLayer
    var nextRecursiveLayout = recursiveLayout

    for layer in minLayer...maxLayer {
        for row in 0..<5 {
            for col in 0..<5 {
                let aliveAdjacents = recursiveAdjacents[row]![col]!
                    .map { (layer: layer + $0.layerDiff, row: $0.row, col: $0.col) }
                    .filter { $0.layer >= minLayer && $0.layer <= maxLayer && recursiveLayout[$0.layer]![$0.row][$0.col] }.count
                if recursiveLayout[layer]![row][col] && aliveAdjacents != 1 {
                    nextRecursiveLayout[layer]![row][col] = false
                } else if !recursiveLayout[layer]![row][col] && (aliveAdjacents == 1 || aliveAdjacents == 2) {
                    nextRecursiveLayout[layer]![row][col] = true
                }
            }
        }
    }

    while minLayer <= maxLayer && isEmptySlice(nextRecursiveLayout[minLayer]!) {
        nextRecursiveLayout[minLayer] = nil
        minLayer += 1
    }
    while minLayer <= maxLayer && isEmptySlice(nextRecursiveLayout[maxLayer]!) {
        nextRecursiveLayout[maxLayer] = nil
        maxLayer -= 1
    }

    recursiveLayout = nextRecursiveLayout
    if minLayer > maxLayer {
        break
    }
}

//printRecursiveLayout(recursiveLayout)
print(recursiveLayout.values.reduce(0) { $0 + $1.flatMap { $0 }.filter { $0 }.count })
