Day 19: Tractor Beam
====================

https://adventofcode.com/2019/day/19

Significantly easier problem than the past few days lmao. Part 1 is just the straightfoward implementation of scan all 2500 points and count up how many of them the VM returns 1 for.

Part 2 takes advantage of the fact that past a certain point (Y > 10 for my input), the tractor beam is made up of a single contiguous slice in every Y coordinate, and the leftmost and rightmost X coordinate of each slice is nondecreasing. This means that given the [left, right] of a slice in row Y, we can find the [left, right] of the slice of row Y + 1 fairly quickly; start at (left, Y + 1) and step right until we're in the tractor beam to find the new left, and start at (right + 1, Y + 1) and step right until we're out of the tractor beam to find the new right (which will be one to the left of the point we stopped). This radically decreases the number of points we have to scan to find the tractor beam, which is probably the point of the problem!

Given a set of 100 consecutive slices, a square of 100x100 can fit in them if the horizontal overlap between the top slice and bottom slice is at least 100 squares, which is just some pretty simple math using the bounds of each slice. So the main loop just cycles through each set of 100 slices, starting from the bottom of the grid we calculated in part 1, until it finds the first one with the required overlap.
