Advent of Code 2019
===================

These are my solutions in Swift to [Advent of Code 2019](https://adventofcode.com/2019), a kind of fun series of 25 programming puzzles that ran in December 2019. I figured I'd throw these up on the web somewhere, because why not. If I was competing for a leaderboard spot that day (that is, I started coding at 9 PM Pacific), then the readme file for that day will give my time and place. Most of the time I wasn't though, been doing other stuff this year instead of being at my computer with Xcode open ready to go at exactly 9 PM.

While the challenges had no time limit, there was a leaderboard that showed (essentially) the fastest 100 solvers of each day's problem, so naturally I tried to solve them as quickly as possible. So, most of the solutions here are simply what left my fingers the fastest. That's speedcoding for you. I hadn't really written much Swift or done much puzzle programming this year, so I was pretty rusty going into this one.

I started writing every single challenge in a playground, because they're fantastic tools, but like last year I just switched to using command-line scripts for everything halfway through because playgrounds massively slow down when you start looping things hundreds of thousands of times. So many VM puzzles, so little time.

Every user had their puzzle inputs randomly selected from a pool of possibilities, so the inputs you see in these files may not be the same ones you'd get were you to sign up. I also tend to preprocess them in a text editor to make them way easier to parse.

* Best placement for any part: day 2, part 2 (159th place)
* Days with later optimizations or bugfixes: 10, 16, 25
* Days that are actually just math problems: 16, 22

*See also: [2015](https://bitbucket.org/CHz16/advent-of-code-2015), [2016](https://bitbucket.org/CHz16/advent-of-code-2016), [2017](https://bitbucket.org/CHz16/advent-of-code-2017), [2018](https://bitbucket.org/CHz16/advent-of-code-2018), [2020](https://bitbucket.org/CHz16/advent-of-code-2020), [2021](https://bitbucket.org/CHz16/advent-of-code-2021), [2022](https://bitbucket.org/CHz16/advent-of-code-2022)*
