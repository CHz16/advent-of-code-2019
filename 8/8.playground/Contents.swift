//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "8", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)

let width = 25, height = 6


// Part 1

var layers: [Substring] = []
var index = input.startIndex
while index < input.endIndex {
    let nextIndex = input.index(index, offsetBy: width * height, limitedBy: input.endIndex) ?? input.endIndex
    layers.append(input[index..<nextIndex])
    index = nextIndex
}
layers.removeLast() // newline string

let digitCounts = layers.map { $0.reduce(into: [:]) { counts, digit in counts[digit, default: 0] += 1 } }
let fewestZeroesLayerCounts = digitCounts.min { $0["0"]! < $1["0"]! }!
print(fewestZeroesLayerCounts["1"]! * fewestZeroesLayerCounts["2"]!)


// Part 2

print("--------------")

let pixelStacks: [[Character]] = layers.reduce(into: Array(repeating: [], count: width * height)) {
    for (n, digit) in $1.enumerated() {
        $0[n].append(digit)
    }
}
let pixels = pixelStacks.map { $0.first { $0 != "2" }! == "1" ? "*" : " " }

for i in stride(from: 0, to: pixels.count, by: width) {
    print(pixels[i..<(i + width)].joined(separator: ""))
}
