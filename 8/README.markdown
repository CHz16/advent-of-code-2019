Day 8: Space Image Format
=========================

https://adventofcode.com/2019/day/8

Oops, someone let me have map/reduce again

The first part splits the image data into layer strings and then reduces each one into a dictionary in order to count the occurrences of each digit. The second part goes through each layer from top to bottom and adds each pixel value to a list for each pixel position, and then selects the first non-transparent pixel from each list to produce the image.
