Day 12: The N-Body Problem
==========================

https://adventofcode.com/2019/day/12

Part 1 is a very straightforward simulation problem, nothing to really comment on there. But part 2 is rough, enough so that I took a big jump up in the standings. I started about 10-15 minutes late or so I wasn't initially going to include my time & placement here, but I am just to show that off. There's a chance I actually could've placed in the top 100 for part 2 if I started on time haha, ah well

For part 2, the key observation is that a body's X position and velocity are only affected by the X positions of the other bodies. The same is of course also true for Y and Z axes of every body. Therefore, the X, Y, and Z axes will all independently cycle, and the system will have returned to its initial state when each axis's cycle coincide; that is, the LCM of the cycle of each axis. Theoretically, the cycle lengths of each axis will be computable in a much shorter time, and they were for my data!

I didn't actually do the LCM calculation in my code, I just punched the cycle lengths into Wolfram Alpha lmao

* Part 1: 992nd place (31:24)
* Part 2: 245th place (49:02)
