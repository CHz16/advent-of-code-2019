#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "12.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

struct Body {
    var x, y, z: Int
    var xVel = 0, yVel = 0, zVel = 0

    var potentialEnergy: Int {
        return (abs(x) + abs(y) + abs(z)) * (abs(xVel) + abs(yVel) + abs(zVel))
    }

    mutating func applyGravity(fromBody otherBody: Body) {
        func gravityAdjustment(_ c1: Int, _ c2: Int) -> Int {
            if c1 == c2 {
                return 0
            } else if c1 < c2 {
                return 1
            } else {
                return -1
            }
        }
        xVel += gravityAdjustment(x, otherBody.x)
        yVel += gravityAdjustment(y, otherBody.y)
        zVel += gravityAdjustment(z, otherBody.z)
    }

    mutating func move() {
        x += xVel
        y += yVel
        z += zVel
    }
}


var initialBodies: [Body] = []
input.enumerateLines { (line, stop) in
    let coordinates = line.components(separatedBy: .whitespaces).compactMap { Int($0) }
    initialBodies.append(Body(x: coordinates[0], y: coordinates[1], z: coordinates[2]))
}

var bodies = initialBodies
for _ in 0..<1000 {
    for a in 0..<(bodies.count - 1) {
        for b in a..<bodies.count {
            if a == b {
                continue
            }
            bodies[a].applyGravity(fromBody: bodies[b])
            bodies[b].applyGravity(fromBody: bodies[a])
        }
    }
    for a in 0..<bodies.count {
        bodies[a].move()
    }
}
print(bodies.map { $0.potentialEnergy }.reduce(0, +))


// Part 2

print("--------------")

bodies = initialBodies
var step = 1
var foundXCycle = false, foundYCycle = false, foundZCycle = false
while !foundXCycle || !foundYCycle || !foundZCycle {
    for a in 0..<(bodies.count - 1) {
        for b in a..<bodies.count {
            if a == b {
                continue
            }
            bodies[a].applyGravity(fromBody: bodies[b])
            bodies[b].applyGravity(fromBody: bodies[a])
        }
    }

    var xReturned = 0, yReturned = 0, zReturned = 0
    for a in 0..<bodies.count {
        bodies[a].move()
        if !foundXCycle && bodies[a].x == initialBodies[a].x && bodies[a].xVel == 0 {
            xReturned += 1
        }
        if !foundYCycle && bodies[a].y == initialBodies[a].y && bodies[a].yVel == 0 {
            yReturned += 1
        }
        if !foundZCycle && bodies[a].z == initialBodies[a].z && bodies[a].zVel == 0 {
            zReturned += 1
        }
    }
    if xReturned == bodies.count {
        print("x cycle", step)
        foundXCycle = true
    }
    if yReturned == bodies.count {
        print("y cycle", step)
        foundYCycle = true
    }
    if zReturned == bodies.count {
        print("z cycle", step)
        foundZCycle = true
    }

    step += 1
}

