//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "1", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

func fuelRequired(forMass mass: Int) -> Int {
    return (mass / 3) - 2
}

print(input.components(separatedBy: .newlines).compactMap { Int($0) }.map { fuelRequired(forMass: $0) }.reduce(0, +))


// Part 2

print("--------------")

func recursiveFuelRequired(forMass mass: Int) -> Int {
    let fuelMass = fuelRequired(forMass: mass)
    if fuelMass > 0 {
        return fuelMass + recursiveFuelRequired(forMass: fuelMass)
    } else {
        return 0
    }
}

print(input.components(separatedBy: .newlines).compactMap { Int($0) }.map { recursiveFuelRequired(forMass: $0) }.reduce(0, +))
