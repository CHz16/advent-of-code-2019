Day 1: The Tyranny of the Rocket Equation
=========================================

https://adventofcode.com/2019/day/1

Very simple math problem to start things off. Part 2 could probably be done cleverly without recursion or looping, but with the values and amount of data provided, that didn't really seem like a thing worth optimizing.
