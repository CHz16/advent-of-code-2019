Day 7: Amplification Circuit
============================

https://adventofcode.com/2019/day/7

Yet another expansion on the VM requires yet another rewrite! This one requires saving state and resuming later with additional inputs so we're actually finally at full-blown VM objects now, like we should've been in the first place. We got there eventually, don't worry about it.

Besides the changes to accept inputs (the array member variable is yet another silly hack lmao) and return outputs/execution state, there's not really much else interesting about this solution. Just does it straightforwardly: goes through every permutation of phase inputs and executes the VMs in order.
