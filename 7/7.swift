#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "7.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


struct VM {
    enum State { case output(Int), waitingForInput, halted }

    var program: [Int]
    var inputs: [Int] = []
    var pc = 0


    func getParameter(atIndex index: Int, withMode mode: Int) -> Int {
        if mode == 0 {
            // mode 0: position
            return program[program[index]]
        } else {
            // mode 1: immediate
            return program[index]
        }
    }

    mutating func execute() -> State {
        outer: while pc < program.count {
            let opcode = program[pc] % 100
            let parameter1Mode, parameter2Mode, parameter3Mode: Int
            if program[pc] > 100 {
                parameter1Mode = (program[pc] / 100) % 10
            } else {
                parameter1Mode = 0
            }
            if program[pc] > 1000 {
                parameter2Mode = (program[pc] / 1000) % 10
            } else {
                parameter2Mode = 0
            }
            if program[pc] > 10000 {
                parameter3Mode = (program[pc] / 10000) % 10
            } else {
                parameter3Mode = 0
            }

            switch opcode {
            case 1:
                // +
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = program[pc + 3]
                program[location] = operand1 + operand2
                pc += 4
            case 2:
                // *
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = program[pc + 3]
                program[location] = operand1 * operand2
                pc += 4
            case 3:
                // input
                if inputs.isEmpty {
                    return .waitingForInput
                } else {
                    let input = inputs.removeFirst()
                    program[program[pc + 1]] = input
                    pc += 2
                }
            case 4:
                // output
                let output = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                pc += 2
                return .output(output)
            case 5:
                // jump if true
                let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                if condition != 0 {
                    pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                } else {
                    pc += 3
                }
            case 6:
                // jump if false
                let condition = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                if condition == 0 {
                    pc = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                } else {
                    pc += 3
                }
            case 7:
                // less than
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = program[pc + 3]
                program[location] = (operand1 < operand2) ? 1 : 0
                pc += 4
            case 8:
                // equals
                let operand1 = getParameter(atIndex: pc + 1, withMode: parameter1Mode)
                let operand2 = getParameter(atIndex: pc + 2, withMode: parameter2Mode)
                let location = program[pc + 3]
                program[location] = (operand1 == operand2) ? 1 : 0
                pc += 4
            case 99:
                // halt
                break outer
            default:
                // oh no
                print("unknown opcode", opcode)
                break outer
            }
        }

        return .halted
    }
}


// Part 1

func permutations<T: Equatable>(_ nums: [T]) -> [[T]] {
    if nums.count == 1 {
        return [nums]
    }

    var s: [[T]] = []
    for n in nums {
        s += permutations(nums.filter { $0 != n }).map { [n] + $0 }
    }
    return s
}

let initialProgram = input.components(separatedBy: ",").compactMap { Int($0) }

var maxSignal = Int.min
for phaseSequence in permutations([0, 1, 2, 3, 4]) {
    var signal = 0
    for phase in phaseSequence {
        var vm = VM(program: initialProgram)
        vm.inputs = [phase, signal]
        if case let .output(value) = vm.execute() {
            signal = value
        }
    }
    if signal > maxSignal {
        print(phaseSequence, signal)
        maxSignal = signal
    }
}


// Part 2

print("--------------")

maxSignal = Int.min
for phaseSequence in permutations([5, 6, 7, 8, 9]) {
    var vms: [VM] = []
    for phase in phaseSequence {
        var vm = VM(program: initialProgram)
        vm.inputs = [phase]
        _ = vm.execute()
        vms.append(vm)
    }

    var signal = 0
    outer: while true {
        for i in 0..<5 {
            vms[i].inputs = [signal]
            if case let .output(value) = vms[i].execute() {
                signal = value
            } else {
                break outer
            }
        }
    }

    if signal > maxSignal {
        print(phaseSequence, signal)
        maxSignal = signal
    }
}
